#!/bin/bash

### go to this dir
cd $(dirname $0)

### make all shell scripts executable (in case that some are not)
find . -name '*.sh' | xargs chmod +x  

### create a temporary directory, backup, etc.
mkdir -p tmp/ db/backup

### set proper ownership to some application files
### that need to be accessed (written) by apache
app_files="modules/menu/menu.xml
           modules/menu/menu_items.js
           modules/menu/edit/menu.xml
           modules/menu/cache/
           db/backup/
           tmp/"
./chown.sh $app_files
chmod g+w  $app_files

### compile the translation messages
l10n/msgfmt.sh en
l10n/msgfmt.sh sq_AL

