
This folder contains some files and folders that are used
to generate documentation about the code of the application,
using doxygen and phpDocumentor.

The files and folders that it contains are:
  doxygen.cfg    -- the configuration for doxygen
  phpdoc_html.sh -- the configuration for phpDocumentor, HTML format
  phpdoc_pdf.sh  -- the configuration for phpDocumentor, PDF format
  generate-code-doc.sh -- script that generates the code documentation
 
To generate the documentation, type: "$ ./generate-code-doc.sh".
