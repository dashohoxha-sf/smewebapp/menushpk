#!/bin/bash
### generate a list of modules from the directory tree

### go to this directory
cd $(dirname $0)

### generate the tree of modules
#tree -d ../modules/ | gawk '$NF!="CVS"' > modules.txt
ls -R -1 ../modules/ \
   | egrep '(^\.\.|.*\.php)' \
   | sed -e 's#^\.\./modules/#+#' -e 's/:$//' -e '/^$/d' -e '/l10n/d' \
   > modules.txt
