
Creating a New Application
==========================

SMEWAPP is a framework for creating web applications for SME-s
(Small and Medium Enterprises). This document explains how to
create a new application based on the framework. The new application
is created by getting a copy of the framework and modifying it.
The modifications of the framework and the development of the
application are stored in Subversion, so that we can easily keep
track of the changes. But we also want time after time to update
the application with the latest release (latest improvments) of
the framework (smewapp). This document explains as well how this 
can be done with svn.

Creating a Project for a New Application
----------------------------------------

Suppose that we are creating a new application for MENU sh.p.k.
We are going to create a project in SVN named 'menushpk'.
It can be created like this:


* Create a Subversion repository for the project (application):
--scr
bash$ svnadmin create /data/svn/menushpk
----


* Export from SVN the latest version of 'smewapp' (the framework):
--scr
bash$ mkdir export
bash$ svn export file:///data/svn/smewapp/trunk export/smewapp
----


* Import the latest version of the framework (smewapp) in the 
  Subversion repository of menushpk, in a branch named 'current':
--scr
bash$ svn mkdir file:///data/svn/menushpk/smewapp
bash$ svn import export/smewapp/ file:///data/svn/menushpk/smewapp/current
bash$ rm -rf export/smewapp/
----


* Create a tag for the current version of smewapp that was imported:
--scr
bash$ svnlook youngest /data/svn/smewapp
27
bash$ svn copy file:///data/svn/menushpk/smewapp/current \
               file:///data/svn/menushpk/smewapp/r27
----


* Copy this revision of smewapp to the main development branch of the
  menushpk, named 'trunk':
--scr
bash$ svn copy file:///data/svn/menushpk/smewapp/r27 \
               file:///data/svn/menushpk/trunk
----


* Checkout the trunk of menushpk:
--scr
bash$ svn checkout file:///data/svn/menushpk/trunk menushpk
----


* Modify and commit it.



Creating a Module From a Module-Template
----------------------------------------
Here will be imported the initial versions of the modules
that are created from the module templates. Then a copy of
the modules will be made in the trunk (main development branch)
which will be then modified and customised further.
This is done in order to keep track of the improvments of the
module templates and to apply them automatically.
See the docs about how this is done.



Updating the Project With the Latest Version of the Framework
-------------------------------------------------------------

Now suppose that the framework 'smewapp' is improved and we
want to apply these improvments in the application 'menushpk'.
It can be done like this:

*

*





Applying the Latest Changes of a Module-Template to a Module
------------------------------------------------------------



Creating a New Application
==========================

SMEWAPP is a framework for creating web applications for SME-s
(Small and Medium Enterprises). This document explains how to
create a new application based on the framework. The new application
is created by getting a copy of the framework and modifying it.
The modifications of the framework and the development of the
application are stored in Subversion, so that we can easily keep
track of the changes. But we also want time after time to update
the application with the latest release (latest improvments) of
the framework (smewapp). This document explains as well how this 
can be done with svn.

Creating a Project for a New Application
----------------------------------------

Suppose that we are creating a new application for MENU sh.p.k.
We are going to create a project in SVN named 'menushpk'.
It can be created like this:


* Create a Subversion repository for the project (application):
--scr
bash$ svnadmin create /data/svn/menushpk
----


* Export from SVN the latest version of 'smewapp' (the framework):
--scr
bash$ mkdir export
bash$ svn export file:///data/svn/smewapp/trunk export/smewapp
----


* Import the latest version of the framework (smewapp) in the 
  Subversion repository of menushpk, in a branch named 'current':
--scr
bash$ svn mkdir file:///data/svn/menushpk/smewapp
bash$ svn import export/smewapp/ file:///data/svn/menushpk/smewapp/current
bash$ rm -rf export/smewapp/
----


* Create a tag for the current version of smewapp that was imported:
--scr
bash$ svnlook youngest /data/svn/smewapp
27
bash$ svn copy file:///data/svn/menushpk/smewapp/current \
               file:///data/svn/menushpk/smewapp/r27
----


* Copy this revision of smewapp to the main development branch of the
  menushpk, named 'trunk':
--scr
bash$ svn copy file:///data/svn/menushpk/smewapp/r27 \
               file:///data/svn/menushpk/trunk
----


* Checkout the trunk of menushpk:
--scr
bash$ svn checkout file:///data/svn/menushpk/trunk menushpk
----


* Modify and commit it.



Creating a Module From a Module-Template
----------------------------------------
Here will be imported the initial versions of the modules
that are created from the module templates. Then a copy of
the modules will be made in the trunk (main development branch)
which will be then modified and customised further.
This is done in order to keep track of the improvments of the
module templates and to apply them automatically.
See the docs about how this is done.



Updating the Project With the Latest Version of the Framework
-------------------------------------------------------------

Now suppose that the framework 'smewapp' is improved and we
want to apply these improvments in the application 'menushpk'.
It can be done like this:

*

*





Applying the Latest Changes of a Module-Template to a Module
------------------------------------------------------------


