-- MySQL dump 8.23
--
-- Host: localhost    Database: menushpk
---------------------------------------------------------
-- Server version	3.23.58

--
-- Current Database: menushpk
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ menushpk;

USE menushpk;

--
-- Table structure for table `doc2payment`
--

DROP TABLE IF EXISTS doc2payment;
CREATE TABLE doc2payment (
  doc_type varchar(100) NOT NULL default '',
  doc_id int(11) NOT NULL default '0',
  payment_id int(11) NOT NULL default '0',
  PRIMARY KEY  (doc_type,doc_id)
) TYPE=MyISAM;

--
-- Table structure for table `garbage`
--

DROP TABLE IF EXISTS garbage;
CREATE TABLE garbage (
  garbage_id int(10) unsigned NOT NULL auto_increment,
  garbage_date date default '0000-00-00',
  close_date date default '0000-00-00',
  timestamp varchar(250) NOT NULL default '',
  garbage_type varchar(100) default '',
  product varchar(100) default '',
  quantity decimal(6,1) default '0.0',
  value decimal(6,1) default '0.0',
  notes text,
  PRIMARY KEY  (garbage_id)
) TYPE=MyISAM;

--
-- Table structure for table `general_expenses`
--

DROP TABLE IF EXISTS general_expenses;
CREATE TABLE general_expenses (
  expense_id int(10) unsigned NOT NULL auto_increment,
  type varchar(100) default '',
  period tinyint(4) default '0',
  value double(11,0) default '0',
  PRIMARY KEY  (expense_id)
) TYPE=MyISAM COMMENT='Expenses that are done monthly or quarterly, like salary pay';

--
-- Table structure for table `ingredients`
--

DROP TABLE IF EXISTS ingredients;
CREATE TABLE ingredients (
  product varchar(20) NOT NULL default '',
  item varchar(20) NOT NULL default '',
  unit varchar(10) default NULL,
  quantity decimal(11,3) default NULL,
  PRIMARY KEY  (item,product)
) TYPE=MyISAM COMMENT='The table that contains ingredients for each product.';

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS items;
CREATE TABLE items (
  item varchar(20) NOT NULL default '',
  unit varchar(10) NOT NULL default '',
  price decimal(6,1) default NULL,
  type varchar(20) default NULL,
  partners text,
  notes text,
  PRIMARY KEY  (item)
) TYPE=MyISAM;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS languages;
CREATE TABLE languages (
  id varchar(10) NOT NULL default '',
  name varchar(100) NOT NULL default '',
  charset varchar(100) NOT NULL default '',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS partners;
CREATE TABLE partners (
  partner_id int(11) NOT NULL auto_increment,
  nickname varchar(20) default NULL,
  firstname varchar(20) NOT NULL default '',
  lastname varchar(20) default NULL,
  e_mail varchar(25) default NULL,
  phone1 varchar(20) default NULL,
  phone2 varchar(20) default NULL,
  address varchar(35) default NULL,
  notes text,
  PRIMARY KEY  (partner_id)
) TYPE=MyISAM;

--
-- Table structure for table `payment_types`
--

DROP TABLE IF EXISTS payment_types;
CREATE TABLE payment_types (
  type_id varchar(100) NOT NULL default '',
  type_name varchar(100) NOT NULL default '',
  PRIMARY KEY  (type_id)
) TYPE=MyISAM;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS payments;
CREATE TABLE payments (
  payment_id int(10) unsigned NOT NULL auto_increment,
  payment_date date default '0000-00-00',
  last_date date default '0000-00-00',
  partner varchar(100) default '',
  type varchar(100) default '',
  amount double default '0',
  percentage_payed smallint(6) default '0',
  close_date date default '0000-00-00',
  timestamp varchar(100) default '',
  notes tinytext,
  PRIMARY KEY  (payment_id)
) TYPE=MyISAM;

--
-- Table structure for table `productions`
--

DROP TABLE IF EXISTS productions;
CREATE TABLE productions (
  production_id int(10) unsigned NOT NULL auto_increment,
  product varchar(100) default '',
  production_date date default '0000-00-00',
  quantity decimal(6,1) default '0.0',
  cost decimal(8,3) default '0.000',
  ingredients text,
  notes text,
  close_date date default '0000-00-00',
  timestamp varchar(100) default '',
  PRIMARY KEY  (production_id)
) TYPE=MyISAM;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS products;
CREATE TABLE products (
  product varchar(20) NOT NULL default '',
  unit varchar(10) NOT NULL default '',
  price decimal(6,1) default NULL,
  validity smallint(6) default NULL,
  partners text,
  notes text,
  PRIMARY KEY  (product)
) TYPE=MyISAM;

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS purchases;
CREATE TABLE purchases (
  purchase_id int(10) unsigned NOT NULL auto_increment,
  purchase_date date default '0000-00-00',
  close_date date default '0000-00-00',
  timestamp varchar(250) NOT NULL default '',
  item varchar(100) default '',
  quantity decimal(6,1) default '0.0',
  price decimal(6,1) default '0.0',
  partner varchar(100) default '',
  notes text,
  PRIMARY KEY  (purchase_id)
) TYPE=MyISAM;

--
-- Table structure for table `refrigerator`
--

DROP TABLE IF EXISTS refrigerator;
CREATE TABLE refrigerator (
  product varchar(20) NOT NULL default '',
  unit varchar(10) default NULL,
  quantity decimal(7,2) default NULL,
  cost decimal(7,2) default NULL,
  PRIMARY KEY  (product)
) TYPE=MyISAM COMMENT='The table that contains .';

--
-- Table structure for table `sellings`
--

DROP TABLE IF EXISTS sellings;
CREATE TABLE sellings (
  selling_id int(10) unsigned NOT NULL auto_increment,
  selling_date date default '0000-00-00',
  timestamp varchar(250) NOT NULL default '',
  close_date date default '0000-00-00',
  product varchar(100) default '',
  quantity decimal(6,1) default '0.0',
  price decimal(6,1) default '0.0',
  partner varchar(100) default '',
  notes text,
  PRIMARY KEY  (selling_id)
) TYPE=MyISAM;

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS units;
CREATE TABLE units (
  unit varchar(20) NOT NULL default '',
  name varchar(100) NOT NULL default '',
  PRIMARY KEY  (unit)
) TYPE=MyISAM COMMENT='The table that contains some item for each product.';

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  user_id int(11) NOT NULL auto_increment,
  username varchar(20) default NULL,
  password varchar(20) NOT NULL default '',
  firstname varchar(20) NOT NULL default '',
  lastname varchar(20) default NULL,
  e_mail varchar(25) default NULL,
  phone1 varchar(20) default NULL,
  phone2 varchar(20) default NULL,
  address varchar(35) default NULL,
  notes text,
  salary smallint(6) default NULL,
  access_rights text,
  PRIMARY KEY  (user_id),
  UNIQUE KEY u (username)
) TYPE=MyISAM;

--
-- Table structure for table `warehouse`
--

DROP TABLE IF EXISTS warehouse;
CREATE TABLE warehouse (
  item varchar(20) NOT NULL default '',
  unit varchar(10) default NULL,
  quantity decimal(7,2) default NULL,
  min_quantity decimal(6,1) default NULL,
  max_quantity decimal(6,1) default NULL,
  price decimal(7,2) default NULL,
  PRIMARY KEY  (item)
) TYPE=MyISAM COMMENT='The table that contains the inventory of the warehouse.';

