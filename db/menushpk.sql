-- MySQL dump 8.23
--
-- Host: localhost    Database: menushpk
---------------------------------------------------------
-- Server version	3.23.58

--
-- Current Database: menushpk
--

-- CREATE DATABASE /*!32312 IF NOT EXISTS*/ menushpk;
-- USE menushpk;

--
-- Table structure for table `doc2payment`
--

DROP TABLE IF EXISTS doc2payment;
CREATE TABLE doc2payment (
  doc_type varchar(100) NOT NULL default '',
  doc_id int(11) NOT NULL default '0',
  payment_id int(11) NOT NULL default '0',
  PRIMARY KEY  (doc_type,doc_id)
) TYPE=MyISAM;

--
-- Dumping data for table `doc2payment`
--


INSERT INTO doc2payment (doc_type, doc_id, payment_id) VALUES ('purchase',32,37),('purchase',33,38),('purchase',34,39),('purchase',35,40),('purchase',36,40),('purchase',37,40),('purchase',38,40),('purchase',39,40),('selling',10,42),('selling',11,42),('selling',12,42),('purchase',40,42),('purchase',41,42),('purchase',42,42),('purchase',43,42),('purchase',44,45),('purchase',45,45),('selling',13,46),('selling',14,46),('purchase',27,49),('purchase',46,49),('selling',15,50);

--
-- Table structure for table `garbage`
--

DROP TABLE IF EXISTS garbage;
CREATE TABLE garbage (
  garbage_id int(10) unsigned NOT NULL auto_increment,
  garbage_date date default '0000-00-00',
  close_date date default '0000-00-00',
  timestamp varchar(250) NOT NULL default '',
  garbage_type varchar(100) default '',
  product varchar(100) default '',
  quantity decimal(6,1) default '0.0',
  value decimal(6,1) default '0.0',
  notes text,
  PRIMARY KEY  (garbage_id)
) TYPE=MyISAM;

--
-- Dumping data for table `garbage`
--


INSERT INTO garbage (garbage_id, garbage_date, close_date, timestamp, garbage_type, product, quantity, value, notes) VALUES (1,'2006-01-01','0000-00-00','1139692510','bad product','pr1',10.0,20.0,'dsd dfd'),(2,'2006-01-01','0000-00-00','1139692542','bad product','pr1',10.0,20.0,'dsd dfd'),(3,'2006-01-01','0000-00-00','1139692564','bad product','pr1',10.0,20.0,'dsd dfd'),(4,'2006-01-01','0000-00-00','1139692651','bad product','pr1',10.0,20.0,'dsd dfd'),(5,'2006-01-01','0000-00-00','1139692446','expired product','pr1',15.0,20.0,'dsd dfd'),(6,'2006-01-01','0000-00-00','1139692459','bad product','pr1',15.0,25.0,'dsd dfd'),(7,'2006-01-01','0000-00-00','1137530854','expired product','pr3',15.0,25.0,'dsd dfd'),(8,'2006-01-01',NULL,'1137530854','expired product','pr3',15.0,25.0,'dsd dfd'),(9,'2006-01-27','0000-00-00','1139692474','expired product','pr1',10.0,15.0,'hdhdh '),(10,'2006-01-27','0000-00-00','1138394521','prodhim i prishur','pr1',10.0,15.0,'hdhdh '),(11,'2006-01-01','0000-00-00','1139776892','bad product','pr2',10.0,20.0,'dsd dfd'),(12,'2006-01-01','0000-00-00','1139776941','expired product','pr2',15.0,20.0,'dsd dfd'),(13,'2006-01-01','0000-00-00','1140121673','bad product','pr2',15.0,20.0,'dsd dfd');

--
-- Table structure for table `general_expenses`
--

DROP TABLE IF EXISTS general_expenses;
CREATE TABLE general_expenses (
  expense_id int(10) unsigned NOT NULL auto_increment,
  type varchar(100) default '',
  period tinyint(4) default '0',
  value double(11,0) default '0',
  PRIMARY KEY  (expense_id)
) TYPE=MyISAM COMMENT='Expenses that are done monthly or quarterly, like salary pay';

--
-- Dumping data for table `general_expenses`
--


INSERT INTO general_expenses (expense_id, type, period, value) VALUES (1,'payment',30,1000),(2,'type1',30,150),(3,'type2',90,300),(4,'type3',100,200);

--
-- Table structure for table `ingredients`
--

DROP TABLE IF EXISTS ingredients;
CREATE TABLE ingredients (
  product varchar(20) NOT NULL default '',
  item varchar(20) NOT NULL default '',
  unit varchar(10) default NULL,
  quantity decimal(11,3) default NULL,
  PRIMARY KEY  (item,product)
) TYPE=MyISAM COMMENT='The table that contains ingredients for each product.';

--
-- Dumping data for table `ingredients`
--


INSERT INTO ingredients (product, item, unit, quantity) VALUES ('1','test6','Kg',8.000),('1','test24','Kg',6.000),('2','i3','u3',8.000),('1','test5','L',7.000),('1','test4','L',5.000),('1','test8','L',9.000),('1','test9','Kg',10.000),('pr1','test4','u4',4.000),('pr1','test5','u5',5.000),('pr1','test6','u6',6.000),('pr1','test7','Kg',7.000),('pr1','test8','L',8.000),('pr1','test9','Lg',0.001),('pr2','item1','',0.500),('pr2','item3','',0.025);

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS items;
CREATE TABLE items (
  item varchar(20) NOT NULL default '',
  unit varchar(10) NOT NULL default '',
  price decimal(6,1) default NULL,
  type varchar(20) default NULL,
  partners text,
  notes text,
  PRIMARY KEY  (item)
) TYPE=MyISAM;

--
-- Dumping data for table `items`
--


INSERT INTO items (item, unit, price, type, partners, notes) VALUES ('item1','Kg',10.0,'ingredient','',''),('item2','L',15.0,'resell','',''),('item3','Kg',20.0,'ingredient','dasho\r\nluli\r\n','');

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS languages;
CREATE TABLE languages (
  id varchar(10) NOT NULL default '',
  name varchar(100) NOT NULL default '',
  charset varchar(100) NOT NULL default '',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Dumping data for table `languages`
--


INSERT INTO languages (id, name, charset) VALUES ('sq_AL','Albanian','iso-8859-1'),('en','English','iso-8859-1');

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS partners;
CREATE TABLE partners (
  partner_id int(11) NOT NULL auto_increment,
  nickname varchar(20) default NULL,
  firstname varchar(20) NOT NULL default '',
  lastname varchar(20) default NULL,
  e_mail varchar(25) default NULL,
  phone1 varchar(20) default NULL,
  phone2 varchar(20) default NULL,
  address varchar(35) default NULL,
  notes text,
  PRIMARY KEY  (partner_id)
) TYPE=MyISAM;

--
-- Dumping data for table `partners`
--


INSERT INTO partners (partner_id, nickname, firstname, lastname, e_mail, phone1, phone2, address, notes) VALUES (1,'dasho','Dashamir','Hoxha','dashohoxha@users.sf.net','','','',''),(4,'luli','','','','','','',''),(3,'taku','Spartak','','','','','',''),(5,'gone','','','','','','','');

--
-- Table structure for table `payment_types`
--

DROP TABLE IF EXISTS payment_types;
CREATE TABLE payment_types (
  type_id varchar(100) NOT NULL default '',
  type_name varchar(100) NOT NULL default '',
  PRIMARY KEY  (type_id)
) TYPE=MyISAM;

--
-- Dumping data for table `payment_types`
--


INSERT INTO payment_types (type_id, type_name) VALUES ('purchase','Purchase'),('selling','Selling'),('salary','Salary'),('other','Other');

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS payments;
CREATE TABLE payments (
  payment_id int(10) unsigned NOT NULL auto_increment,
  payment_date date default '0000-00-00',
  last_date date default '0000-00-00',
  partner varchar(100) default '',
  type varchar(100) default '',
  amount double default '0',
  percentage_payed smallint(6) default '0',
  close_date date default '0000-00-00',
  timestamp varchar(100) default '',
  notes tinytext,
  PRIMARY KEY  (payment_id)
) TYPE=MyISAM;

--
-- Dumping data for table `payments`
--


INSERT INTO payments (payment_id, payment_date, last_date, partner, type, amount, percentage_payed, close_date, timestamp, notes) VALUES (1,'2005-12-19','2005-12-22','dasho','cash',100,50,NULL,'1135111480','ssf fgg\r\nkdkd\r\ndlhdhdh'),(2,'2005-12-19','2005-12-22','dasho','cash',100,50,NULL,'1135111480','ssf fgg\r\nkdkd\r\ndlhdhdh'),(3,'2005-12-20','0000-00-00','dsdf','fggf',12,0,NULL,'1135111667',''),(4,'2005-12-20','2005-12-20','','',0,0,NULL,'1135111678',''),(5,'2005-12-21','2005-12-21','','',0,100,'0000-00-00','1135197897',NULL),(6,'2005-12-21','2005-12-21','','',0,100,'0000-00-00','1135197992',NULL),(7,'2005-12-21','2005-12-21','','',0,100,NULL,'1135198480',NULL),(8,'2005-12-15','2005-12-15','test1','',0,100,NULL,'1135198602',NULL),(9,'2005-12-22','2005-12-22','','',0,100,NULL,'1135288906',NULL),(10,'2005-12-22','2005-12-22','','',0,100,NULL,'1135288930',NULL),(11,'2005-12-22','2005-12-22','','',0,100,NULL,'1135289040',NULL),(12,'2005-12-22','2005-12-22','','',0,100,NULL,'1135289147',NULL),(13,'2005-12-22','2005-12-22','','',0,100,NULL,'1135289235',NULL),(14,'2005-12-26','2005-12-26','4','',100,100,NULL,'1135629846',' '),(15,'0000-00-00','0000-00-00','{partner_id}','',0,100,NULL,'1135630300',NULL),(16,'0000-00-00','0000-00-00','{partner_id}','',0,100,NULL,'1135630356',NULL),(17,'0000-00-00','0000-00-00','{partner_id}','',0,100,NULL,'1135630559',NULL),(18,'0000-00-00','0000-00-00','{partner_id}','',0,100,NULL,'1135630647',NULL),(19,'0000-00-00','0000-00-00','{partner_id}','',0,100,NULL,'1135630832',NULL),(20,'0000-00-00','0000-00-00','{partner_id}','',0,100,NULL,'1135630868',NULL),(21,'2005-12-26','2005-12-26','4','',0,100,NULL,'1135630903',NULL),(22,'2005-12-26','2005-12-26','4','',0,100,NULL,'1135630947',NULL),(23,'2006-01-06','2006-01-06','4','',100,50,NULL,'1136575636',' '),(24,'2005-12-26','2005-12-26','','',0,100,NULL,'1137222402',NULL),(25,'2006-01-14','2006-01-14','gone','',0,100,NULL,'1137222449',NULL),(26,'2006-01-14','2006-01-14','dasho','',0,100,NULL,'1137222586',NULL),(27,'2006-01-14','2006-01-14','luli','blerje',0,100,NULL,'1137222681',NULL),(28,'2006-01-15','2006-01-15','luli','purchase',0,100,NULL,'1137296352',NULL),(29,'2006-01-15','2006-01-15','dasho','purchase',0,100,NULL,'1137296373',NULL),(37,'2006-01-15','2006-01-15','luli','',0,100,NULL,'1137352201',NULL),(38,'2006-01-15','2006-01-15','','',0,100,NULL,'1137352311',NULL),(39,'2006-01-15','2006-01-15','','',0,100,NULL,'1137353849',NULL),(40,'2006-01-15','2006-01-15','taku','',0,100,NULL,'1137353880',NULL),(46,'2006-01-27','2006-01-27','dasho','',0,100,NULL,'1138391425',NULL),(42,'2006-01-15','2006-01-15','luli','',0,100,NULL,'1137356943',NULL),(45,'2006-01-27','2006-01-27','','',0,100,NULL,'1138390728',NULL),(47,'2006-01-27','2006-01-27','taku','selling',20,0,'0000-00-00','1138393779','jsjsj'),(48,'2006-01-27','2006-01-27','taku','selling',20,20,'2006-02-14','1139954211','jsjsj'),(49,'2006-01-15','2006-01-15','gone','selling',50,100,'2006-02-14','1139954637',' '),(50,'2006-02-10','2006-02-10','taku','',0,100,NULL,'1139613150',NULL),(51,'2006-02-15','2006-02-15','dasho','purchase',100,10,NULL,'1140038009','');

--
-- Table structure for table `productions`
--

DROP TABLE IF EXISTS productions;
CREATE TABLE productions (
  production_id int(10) unsigned NOT NULL auto_increment,
  product varchar(100) default '',
  production_date date default '0000-00-00',
  quantity decimal(6,1) default '0.0',
  cost decimal(8,3) default '0.000',
  ingredients text,
  notes text,
  close_date date default '0000-00-00',
  timestamp varchar(100) default '',
  PRIMARY KEY  (production_id)
) TYPE=MyISAM;

--
-- Dumping data for table `productions`
--


INSERT INTO productions (production_id, product, production_date, quantity, cost, ingredients, notes, close_date, timestamp) VALUES (1,'kos','2005-12-23',100.0,0.000,'','',NULL,'1135371985'),(2,'kos','2005-12-23',100.0,0.000,'','',NULL,'1135371985'),(3,'pr2','2005-12-23',100.0,0.000,'','',NULL,'1137616984'),(4,'pr2','2006-01-18',5.0,0.000,'','',NULL,'1137616999'),(5,'pr2','2006-01-18',5.0,0.000,'','',NULL,'1137617034'),(6,'pr2','2006-01-18',5.0,0.000,'','',NULL,'1137617066'),(7,'pr2','2006-01-18',5.0,0.000,'','',NULL,'1137617111'),(8,'pr1','2006-01-18',0.0,0.000,'test4=4\ntest5=5\ntest6=6\ntest7=7\ntest8=8\ntest9=9\n','',NULL,'1137784333'),(9,'pr3','2006-01-20',20.0,130.000,'item2=4\r\nitem3=5\r\nitem4=6\r\ntest7=7\r\ntest8=8\r\ntest9=9\r\n','','2006-02-12','1139785940'),(10,'pr1','2006-01-27',10.0,41.200,'item1=4.000\r\nitem2=5.000\r\ntest6=6.000\r\ntest7=7.000\r\ntest8=8.000\r\ntest9=0.001\r\n','dfdfd','2006-02-12','1139785885'),(11,'pr1','2006-01-27',10.0,0.000,'test4=4\ntest5=5\ntest6=6\ntest7=7\ntest8=8\ntest9=9\n','dfdfd','0000-00-00','1138393094'),(12,'pr2','2006-01-31',10.0,3.500,'item1=0.300\r\nitem3=0.025\r\ntest=0.01','','0000-00-00','1138846242'),(13,'pr1','2006-03-08',0.0,0.000,'','',NULL,'1141811095');

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS products;
CREATE TABLE products (
  product varchar(20) NOT NULL default '',
  unit varchar(10) NOT NULL default '',
  price decimal(6,1) default NULL,
  validity smallint(6) default NULL,
  partners text,
  notes text,
  PRIMARY KEY  (product)
) TYPE=MyISAM;

--
-- Dumping data for table `products`
--


INSERT INTO products (product, unit, price, validity, partners, notes) VALUES ('pr1','Kg',0.0,0,'',''),('pr2','Kg',0.0,0,'',''),('pr3','Kg',0.0,0,'',''),('pr4','Kg',0.0,0,'','');

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS purchases;
CREATE TABLE purchases (
  purchase_id int(10) unsigned NOT NULL auto_increment,
  purchase_date date default '0000-00-00',
  close_date date default '0000-00-00',
  timestamp varchar(250) NOT NULL default '',
  item varchar(100) default '',
  quantity decimal(6,1) default '0.0',
  price decimal(6,1) default '0.0',
  partner varchar(100) default '',
  notes text,
  PRIMARY KEY  (purchase_id)
) TYPE=MyISAM;

--
-- Dumping data for table `purchases`
--


INSERT INTO purchases (purchase_id, purchase_date, close_date, timestamp, item, quantity, price, partner, notes) VALUES (1,'2005-12-18',NULL,'1135628357','',10.0,45.0,'hssh','sffg'),(2,'2005-12-18','2006-02-05','1134931809','qumesht',10.0,45.0,'hssh','sffg'),(3,'2005-12-18',NULL,'1134931809','qumesht',10.0,45.0,'hssh','sffg'),(4,'2005-12-18',NULL,'1134931809','qumesht',10.0,45.0,'hssh','sffg'),(5,'2005-12-18',NULL,'1134931957','domate',20.0,10.0,'gdgf','gfgfg'),(6,'2005-12-18',NULL,'1134931957','domate',20.0,10.0,'gdgf','gfgfg'),(7,'2005-12-18',NULL,'1134931957','domate',20.0,10.0,'gdgf','gfgfg'),(9,'2005-12-15',NULL,'1135199510','test1',10.0,20.0,'test1',''),(10,'2005-12-21',NULL,'1135191896','',0.0,0.0,'',''),(11,'2005-12-15',NULL,'1137617641','test',10.0,20.0,'dasho',''),(12,'2005-12-21',NULL,'1135197892','',0.0,0.0,'',''),(13,'2005-12-22',NULL,'1135289143','test',0.0,0.0,'',''),(14,'2005-12-26',NULL,'1135628408','qumesht',0.0,0.0,'',''),(15,'2005-12-26',NULL,'1135629072','qumesht',0.0,0.0,'1',''),(16,'2005-12-26',NULL,'1135629796','',0.0,13.0,'4',' '),(17,'2006-01-06','2006-02-05','1139170197','item1',10.0,12.0,'luli',''),(18,'2006-01-06','2006-02-05','1139170212','item1',10.0,12.0,'taku',''),(19,'2006-01-06','2006-02-05','1139170225','item1',10.0,12.0,'dasho',''),(20,'2006-01-06','2006-02-05','1139170239','item1',10.0,15.0,'dasho',''),(21,'2006-01-14','2006-02-05','1139170254','item1',5.0,10.0,'gone',''),(22,'2006-01-14','2006-02-05','1139170268','item1',10.0,5.0,'dasho',''),(23,'2006-01-14','2006-02-05','1138907815','item1',7.0,8.0,'luli',''),(24,'2006-01-14','2006-02-05','1138733327','item1',0.0,0.0,'taku',''),(26,'2006-01-15','2006-02-05','1137296020','test23',0.0,0.0,'luli',''),(27,'2006-01-15','2006-02-05','1138656574','item2',20.0,20.0,'gone',''),(28,'2006-01-15','2006-02-05','1139170297','item1',0.0,0.0,'luli',''),(29,'2006-01-15','2006-02-05','1139170310','item1',0.0,0.0,'dasho',''),(30,'2006-01-15','2006-02-05','1139170331','item1',0.0,0.0,'taku',''),(31,'2006-01-15','2006-02-05','1139170350','item2',0.0,0.0,'dasho',''),(32,'2006-01-15','2006-02-05','1139170369','item2',0.0,0.0,'luli',''),(33,'2006-01-15','2006-02-05','1139170387','item2',0.0,0.0,'',''),(34,'2006-01-15','2006-02-05','1139170408','item3',0.0,0.0,'',''),(35,'2006-01-15','2006-02-05','1139170429','item3',0.0,0.0,'taku',''),(36,'2006-01-15','2006-02-05','1139170445','item3',0.0,0.0,'',''),(37,'2006-01-15','2006-02-05','1139170461','item3',0.0,0.0,'',''),(38,'2006-01-15','2006-02-05','1139170490','item3',0.0,0.0,'',''),(39,'2006-01-15','2006-02-05','1139170533','item1',0.0,0.0,'',''),(40,'2006-01-22','2006-02-05','1139170824','item2',0.0,0.0,'',''),(41,'2006-01-27',NULL,'1138390383','item2',10.0,0.0,'taku',''),(42,'2006-01-27',NULL,'1138390575','item3',0.0,0.0,'',''),(43,'2006-01-27',NULL,'1138390579','item3',0.0,0.0,'',''),(44,'2006-01-27',NULL,'1138390750','item1',10.0,20.0,'gone','gfg'),(45,'2006-01-27','2006-02-05','1138390750','item1',10.0,20.0,'gone','gfg'),(46,'2006-01-30','2006-02-05','1138653122','item2',15.0,20.0,'','');

--
-- Table structure for table `refrigerator`
--

DROP TABLE IF EXISTS refrigerator;
CREATE TABLE refrigerator (
  product varchar(20) NOT NULL default '',
  unit varchar(10) default NULL,
  quantity decimal(7,2) default NULL,
  cost decimal(7,2) default NULL,
  PRIMARY KEY  (product)
) TYPE=MyISAM COMMENT='The table that contains .';

--
-- Dumping data for table `refrigerator`
--


INSERT INTO refrigerator (product, unit, quantity, cost) VALUES ('1','ddd',0.00,0.00),('test','L',0.00,0.00),('test1','Kg',10.00,5.00),('test2','L',1.00,2.00),('pr2','Kg',10.00,6.74),('item2','L',80.00,18.70),('pr1','Kg',10.00,41.20),('pr3','Kg',20.00,130.00);

--
-- Table structure for table `sellings`
--

DROP TABLE IF EXISTS sellings;
CREATE TABLE sellings (
  selling_id int(10) unsigned NOT NULL auto_increment,
  selling_date date default '0000-00-00',
  timestamp varchar(250) NOT NULL default '',
  close_date date default '0000-00-00',
  product varchar(100) default '',
  quantity decimal(6,1) default '0.0',
  price decimal(6,1) default '0.0',
  partner varchar(100) default '',
  notes text,
  PRIMARY KEY  (selling_id)
) TYPE=MyISAM;

--
-- Dumping data for table `sellings`
--


INSERT INTO sellings (selling_id, selling_date, timestamp, close_date, product, quantity, price, partner, notes) VALUES (1,'2005-12-19','1135025776',NULL,'djath',10.0,15.0,'dsf',''),(2,'2005-12-19','1135025776',NULL,'djath',10.0,15.0,'dsf',''),(3,'2005-12-19','1135025776',NULL,'djath',10.0,15.0,'dsf',''),(4,'2005-12-19','1135025795',NULL,'djath',10.0,16.0,'dsf',''),(5,'2005-12-19','1135025822',NULL,'sallate ruse',10.0,16.0,'dsf',''),(6,'2005-12-19','1135025822',NULL,'sallate ruse',10.0,16.0,'dsf',''),(7,'2005-12-22','1135288834',NULL,'test',0.0,0.0,'',''),(8,'2005-12-22','1135288926',NULL,'test1',0.0,0.0,'',''),(9,'2005-12-22','1135289232',NULL,'test3',0.0,0.0,'',''),(10,'2006-01-15','1137617866',NULL,'pr1',0.0,0.0,'luli',''),(11,'2006-01-15','1137357023',NULL,'',0.0,0.0,'',''),(12,'2006-01-15','1139954183',NULL,'pr1',30.0,0.0,'taku',''),(13,'2006-01-27','1138735349','2006-02-10','pr2',15.0,30.0,'dasho','jddj'),(14,'2006-01-27','1138391460','0000-00-00','pr2',15.0,20.0,'dasho','jddj'),(15,'2006-02-10','1139613150','2006-02-10','pr1',5.0,15.0,'taku','');

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS units;
CREATE TABLE units (
  unit varchar(20) NOT NULL default '',
  name varchar(100) NOT NULL default '',
  PRIMARY KEY  (unit)
) TYPE=MyISAM COMMENT='The table that contains some item for each product.';

--
-- Dumping data for table `units`
--


INSERT INTO units (unit, name) VALUES ('Kg','Kilogram'),('L','Liter');

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  user_id int(11) NOT NULL auto_increment,
  username varchar(20) default NULL,
  password varchar(20) NOT NULL default '',
  firstname varchar(20) NOT NULL default '',
  lastname varchar(20) default NULL,
  e_mail varchar(25) default NULL,
  phone1 varchar(20) default NULL,
  phone2 varchar(20) default NULL,
  address varchar(35) default NULL,
  notes text,
  salary smallint(6) default NULL,
  access_rights text,
  PRIMARY KEY  (user_id),
  UNIQUE KEY u (username)
) TYPE=MyISAM;

--
-- Dumping data for table `users`
--


INSERT INTO users (user_id, username, password, firstname, lastname, e_mail, phone1, phone2, address, notes, salary, access_rights) VALUES (1,'dasho','14Ilexm30AIL6','Dashamir','Hoxha','dhoxha@inima.al','','','Lek Dukagjini 3','',0,'documents/purchases/list,documents/productions/list,documents/sellings/list,documents/payments/list,documents/garbage/list,inventory/warehouse,inventory/refrigerator,reports/purchases,reports/productions,reports/sellings,reports/payments,reports/garbage,reports/balance,tables/items,tables/products,tables/general_expenses,tables/simple_tables/group1,admin/users,admin/partners,backup,documents/purchases/new,documents/purchases/edit,documents/productions/new,documents/productions/edit,documents/sellings/new,documents/sellings/edit,documents/payments/new,documents/payments/edit,documents/garbage/new,documents/garbage/edit,user_profile'),(2,'luli','14lXLrTV7mt1s','Luli','Hoxha','','','','','',0,'documents/purchases/list,documents/productions/list,documents/sellings/list,documents/payments/list,magazina,frigoriferi,koshi,tables/items,tables/products,tables/general_expenses,admin/users,admin/partners,backup,documents/purchases/new,documents/purchases/edit,documents/productions/new,documents/productions/edit,documents/sellings/new,documents/sellings/edit,documents/payments/new,documents/payments/edit,user_profile'),(3,'taku','60OIfxC50daJA','Spartak','','','','','','',0,NULL);

--
-- Table structure for table `warehouse`
--

DROP TABLE IF EXISTS warehouse;
CREATE TABLE warehouse (
  item varchar(20) NOT NULL default '',
  unit varchar(10) default NULL,
  quantity decimal(7,2) default NULL,
  min_quantity decimal(6,1) default NULL,
  max_quantity decimal(6,1) default NULL,
  price decimal(7,2) default NULL,
  PRIMARY KEY  (item)
) TYPE=MyISAM COMMENT='The table that contains the inventory of the warehouse.';

--
-- Dumping data for table `warehouse`
--


INSERT INTO warehouse (item, unit, quantity, min_quantity, max_quantity, price) VALUES ('item1','Kg',101.00,0.0,0.0,10.30),('item2','Kg',-120.00,4.0,7.0,0.00),('item3','L',-98.00,0.0,0.0,20.00),('item4','Kg',-115.00,6.0,10.0,5.00),('item5','L',1.00,2.0,7.0,1.00);

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS session;
CREATE TABLE session (
  id varchar(255) NOT NULL default '',
  vars text NOT NULL,
  PRIMARY KEY  (id)
) TYPE=MyISAM;

