#!/bin/bash
### dump some tables from a database to another

if [[ $# -lt 3 ]]
then
  echo "Usage: $0 db1 db2 [table]+"
  echo "Dump the given tables from the db1 to db2"
  exit 0
fi

db1=$1
db2=$2
shift
shift

host=localhost
user=root

### dump the database $dbname into the file smewapp.sql
mysqldump --add-drop-table --allow-keyword \
          --complete-insert --extended-insert \
          --compress --host=$host --user=$user \
          $db1 "$@" \
  | mysql --user=$user --host=$host $db2

