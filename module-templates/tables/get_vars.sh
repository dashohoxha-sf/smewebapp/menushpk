
### get the values of the variables

if [ -f $destination/vars ]
then
  . $destination/vars
fi

if [ -z "$appname" ]
then
  read -p "appname[appname]=" appname
  appname=${appname:-appname}
fi

if [ -z "$package" ]
then
  read -p "package[package]=" package
  package=${package:-package}
fi

if [ -z "$module" ]
then
  read -p "module[tables]=" module
  module=${module:-tables}
fi

if [ -z "$tables" ]
then
  read -p "tables[$module]=" tables
  tables=${tables:-$module}
fi

if [ -z "$table" ]
then
  read -p "table[table]=" table
  table=${table:-table}
fi
