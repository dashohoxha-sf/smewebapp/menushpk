
This module template can be used for displaying and editing simple small
tables, e.g. coefficient tables, auxiliary tables (that can be used for
listbox-es, etc.). The tables can be grouped in several groups, where each
group has its own interface (menu item); this allows more flexibility in
giving access permissions to the users.

In order to make a module from this module template, these parameters of
the module template must be given:
* appname
* package
* module
* tables
* table

A new module is created by using the script 'create_module.sh':
-scr
bash$ tables/create_module.sh
Usage: tables/create_module.sh destination
Create a new module by copying the module template to the
given destination directory, and replacing the variables in it.
----

--scr
bash$ tables/create_module.sh ../test/dir
----
