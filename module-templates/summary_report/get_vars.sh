
### get the values of the variables

if [ -f $destination/vars ]
then
  . $destination/vars
fi

if [ -z "$appname" ]
then
  read -p "appname[appname]=" appname
  appname=${appname:-appname}
fi

if [ -z "$package" ]
then
  read -p "package[package]=" package
  package=${package:-package}
fi

if [ -z "$module" ]
then
  read -p "module[module]=" module
  module=${module:-module}
fi

if [ -z "$dbtable" ]
then
  read -p "dbtable[$module]=" dbtable
  dbtable=${dbtable:-$module}
fi

if [ -z "$document" ]
then
  read -p "document[$module]=" document
  document=${document:-$module}
fi

if [ -z "$documents" ]
then
  read -p "documents[$module]=" documents
  documents=${documents:-$module}
fi
