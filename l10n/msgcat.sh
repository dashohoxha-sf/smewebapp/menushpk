#!/bin/bash
### concaenate translation files (*.po) to a compendium file

### go to this directory
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 ll_CC"
  echo "where ll_CC is the language code, like en_US or sq_AL"
  exit 1
fi

lng=$1

app_name=$(./get_app_name.sh)
mv compendium-$lng.po compendium-$lng-old.po
msgcat --output-file=compendium-$lng.po --no-location compendium-$lng-old.po \
       $lng/LC_MESSAGES/$app_name.po \
       ../modules/documents/l10n/$lng/LC_MESSAGES/documents.po \
       ../modules/admin/l10n/$lng/LC_MESSAGES/admin.po \
       ../modules/inventory/l10n/$lng/LC_MESSAGES/inventory.po \
       ../modules/tables/l10n/$lng/LC_MESSAGES/tables.po

