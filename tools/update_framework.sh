#!/bin/bash
### get the latest version of the smewapp framework
### and apply it to the application

smewapp=/data/svn/smewapp
app=/data/svn/menushpk
svn_load_dirs=/usr/share/doc/subversion-1.3.2/svn_load_dirs.pl

### go to this dir
cd $(dirname $0)

### check that this is not the framework itself, but an application
is_smewapp=$(svn info | grep URL | grep smewapp)
if [ "$is_smewapp" != "" ]
then
  echo "This is the framework itself, cannot be updated."
  exit 1
fi

### get the latest revision number of the framework
rev=$(svnlook youngest $smewapp/)

### export the latest version of the framework
svn export file://$smewapp/trunk latest

### import in the application
$svn_load_dirs -t r$rev file://$app/smewapp current latest/

### clean
rm -rf latest/

### merge changes
svn ls file:///data/svn/$app/smewapp
echo "Now run:"
echo "svn merge file://$app/smewapp/{...,r$rev} .."

