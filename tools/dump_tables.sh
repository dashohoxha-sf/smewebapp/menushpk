#!/bin/bash
### dump some tables from a database to another

if [[ $# -lt 2 ]]
then
  echo "Usage: $0 db [table]+"
  echo "Dump the given tables from the db to a file."
  exit 0
fi

db=$1
shift

host=localhost
user=root

### dump the database $dbname into the file smewapp.sql
mysqldump --add-drop-table --allow-keyword \
          --no-data --host=$host --user=$user \
          $db "$@" > dbtables.sql

