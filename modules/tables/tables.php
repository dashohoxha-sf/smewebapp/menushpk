<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package tables
 */
class tables extends WebObject
{
  function init()
  {
    $this->addSVar('module', 'edit_tables.html');
  }

  function onParse()
    {
      global $event;
      if ($event->target=='main' and $event->name=='select')
        {
          $this->select($event->args);
        }
    }

  function select($event_args)
    {
      $interface = WebApp::getSVar('interface');
      switch ($interface)
        {
          //items
        case 'tables/items':
          $interface_title = T_("Items");
          $module = 'items/items.html';
          break;

          //products
        case 'tables/products':
          $interface_title = T_("Products");
          $module = 'products/products.html';
          break;

          //general_expenses
        case 'tables/general_expenses':
          $interface_title = T_("Expenses");
          $module = 'general_expenses/expenseList.html';
          break;

          //simple_tables
        case 'tables/simple_tables/group1':
        case 'tables/simple_tables/group2':
        case 'tables/simple_tables/group3':
          $interface_title = T_("Simple Tables");
          $module = 'simple_tables/simple_tables.html';
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);
      $this->setSVar('module', $module);
    }
}
?>
