-- MySQL dump 8.23
--
-- Host: localhost    Database: menushpk
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `ingredients`
--

DROP TABLE IF EXISTS ingredients;
CREATE TABLE ingredients (
  product varchar(20) NOT NULL default '',
  item varchar(20) NOT NULL default '',
  unit varchar(10) default NULL,
  quantity decimal(11,3) default NULL,
  PRIMARY KEY  (item,product)
) TYPE=MyISAM COMMENT='The table that contains ingredients for each product.';

