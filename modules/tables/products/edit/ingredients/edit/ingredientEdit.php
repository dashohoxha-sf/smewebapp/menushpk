<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    tables
 * @subpackage products
 */
include_once FORM_PATH.'formWebObj.php';

class ingredientEdit extends formWebObj
{
  function init()
    {
      $this->addSVar('mode', 'hidden');  // add | edit | hidden
      $this->addSVar('item', UNDEFINED);
    }

  function on_save($event_args)
    {
      $record = $event_args;
      $record['product'] = WebApp::getSVar('ingredientList->product');
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //add the new ingredient
          $this->insert_record($record, 'ingredients');

          //set 'ingredients_rs->recount' to 'true'
          //so that the records are counted again
          WebApp::setSVar('ingredients_rs->recount', 'true');
        }
      else if ($mode=='edit')
        {
          $record['product'] = WebApp::getSVar('ingredientList->product');
          $record['item'] = $this->getSVar('item');
          $this->update_record($record, 'ingredients', 'product,item');
        }

      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');
      $this->setSVar('item', UNDEFINED);
    }

  function on_cancel($event_args)
    {
      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');
      $this->setSVar('item', UNDEFINED);
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //add empty variables for each field in the table
          $vars = $this->pad_record(array(), 'ingredients');
          WebApp::addVars($vars);
        }
      else if ($mode=='edit')
        {
          $rs = WebApp::openRS('get_ingredient');
          $vars = $rs->Fields();
          WebApp::addVars($vars);
        }
    }
}
?>