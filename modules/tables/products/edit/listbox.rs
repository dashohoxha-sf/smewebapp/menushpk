<!--# -*-SQL-*- tell emacs to use SQL mode #-->

<!--# used for the listbox of partners #-->
<Recordset ID="partner_list">
  <Query> SELECT partner_id AS id, nickname AS label FROM partners </Query>
</Recordset>

<!--# used for the listbox of units #-->
<Recordset ID="units">
  <Query> SELECT unit AS id, unit AS label FROM units </Query>
</Recordset>
