<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


include_once FORM_PATH.'formWebObj.php';

/**
 * @package    documents
 * @subpackage productions
 */
class productionEdit extends formWebObj
{
  function init()
    {
      $this->addSVar('mode', 'add');  //add or edit
      $this->addSVar('production_id', UNDEFINED);
      $this->addSVar('closed', 'false');
    }

  function onParse()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->setSVar('closed', 'false');
        }
      else
        {
          $rs = WebApp::openRS('get_production');
          $close_date = $rs->Field('close_date');
          $closed = ($close_date==NULL_VALUE ? 'false' : 'true');
          $this->setSVar('closed', $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //add empty variables for each field in the table
          $production = $this->pad_record(array(), 'productions');
          WebApp::addVars($production);

          //title
          $title = T_("New Production");
        }
      else if ($mode=='edit')
        {
          $rs = WebApp::openRS('get_production');
          $fields = $rs->Fields();
          WebApp::addVars($fields);

          //title
          $production_id = $fields['production_id'];
          $title = T_("Production").": $production_id";

          //ingredients

          //modification_date
          $timestamp = $fields['timestamp'];
          $modification_date = date('d/m/Y', $timestamp);
          WebApp::addVar('modification_date', $modification_date);
        }

      WebApp::addVar('title', $title);
    }

  function on_save($event_args)
    {
      $record = $event_args;
      $record['timestamp'] = time();
      if ($record['production_date']=='')
        $record['production_date'] = get_curr_date('Y-m-d');

      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->add_new_production($record);
        }
      else //mode=='edit'
        {
          $recalculate = $record['recalculate'];
          if ($recalculate=='true')
            {
              $product = $record['product'];
              $record['ingredients'] = $this->get_ingredients($product);
            }
          $record['production_id'] = $this->getSVar('production_id');
          $this->update_record($record, 'productions', 'production_id');
        }

      //calculate the cost of the production and save it
      $this->calculate_production_cost();
    }

  /** create a new production with almost the same data as this one */
  function on_clone($event_args)
    {
      $rs = WebApp::openRS('get_production');
      $record = $rs->Fields();
      unset($record['production_id']);
      $this->add_new_production($record);
    }

  function add_new_production($production)
    {
      //add the new production
      $production['close_date'] = 'NULL';
      $product = $production['product'];
      $production['ingredients'] = $this->get_ingredients($product);
      $this->insert_record($production, 'productions');

      //after a project is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar('mode', 'edit');

      //set 'productions_rs->recount' to 'true'
      //so that the records are counted again
      //and the new production is displayed at the end of the list
      WebApp::setSVar('productions_rs->recount', 'true');

      //set the new production_id
      $rs = WebApp::openRS('get_last_production_id');
      $production_id = $rs->Field('last_production_id');
      $this->setSVar('production_id', $production_id);
    }

  function get_ingredients($product)
    {
      $ingredients = '';
      $rs = WebApp::openRS('get_ingredients', compact('product'));
      while (!$rs->EOF())
        {
          $item = $rs->Field('item');
          $quantity = $rs->Field('quantity');
          $ingredients .= "$item=$quantity\n";
          $rs->MoveNext();
        }
      return $ingredients;
    }

  /**
   * Returns an associated array of ingredients and their
   * quantities. $str_ingredients is a string of ingredients.
   */
  function get_ingredients_array($str_ingredients)
    {
      $arr_ingredients = array();
      $arr_lines = explode("\n", $str_ingredients);
      for ($i=0; $i < sizeof($arr_lines); $i++)
        {
          $line = $arr_lines[$i];
          list($item,$quantity) = split('=', $line);
          if ($item=='')  continue;
          $arr_ingredients[$item] = $quantity;
        }

      return $arr_ingredients;
    }

  function on_close($event_args)
    {
      $this->remove_from_warehouse();
      $this->add_to_refrigerator();

      $params['close_date'] = get_curr_date('Y-m-d');
      WebApp::execDBCmd('close_production', $params);
    }

  function calculate_production_cost()
    {
      $production = WebApp::openRS('get_production');
      $ingredients = $production->Field('ingredients');
      $arr_ingredients = $this->get_ingredients_array($ingredients);
      
      //the cost of the product is the sum of costs of each ingredient
      $cost = 0.0;
      while (list($item,$quantity) = each($arr_ingredients))
        {
          //get the price of the item in the warehouse
          $w_item = WebApp::openRS('get_warehouse_item', compact('item'));
          $w_price = $w_item->Field('price');
          if ($w_price==UNDEFINED)  $w_price = 0.0;

          $cost += $quantity * $w_price;
        }

      //save the cost in the DB
      WebApp::execDBCmd('update_cost', compact('cost'));
    }

  function remove_from_warehouse()
    {
      $production = WebApp::openRS('get_production');
      $p_quantity = $production->Field('quantity');
      $ingredients = $production->Field('ingredients');
      $arr_ingredients = $this->get_ingredients_array($ingredients);

      while (list($item,$i_quantity) = each($arr_ingredients))
        {
          $w_item = WebApp::openRS('get_warehouse_item', compact('item'));
          if ($w_item->EOF()) continue;
          $w_quantity = $w_item->Field('quantity');
          $quantity = $w_quantity - $p_quantity*$i_quantity;
          $arr_w_item = compact('item', 'quantity');
          WebApp::execDBCmd('update_warehouse_item', $arr_w_item);
        }
    }

  function add_to_refrigerator()
    {
      $production = WebApp::openRS('get_production');
      $r_prod = WebApp::openRS('get_refrigerator_product',
                               array('product'=>$production->Field('product')));
      if ($r_prod->EOF())
        {
          WebApp::execDBCmd('add_refrigerator_product', $production->Fields());
        }
      else
        {
          $r_quantity = $r_prod->Field('quantity');
          $p_quantity = $production->Field('quantity');
          $r_cost = $r_prod->Field('cost');
          $p_cost = $production->Field('cost');
          $cost = ( ($r_quantity * $r_cost + $p_quantity * $p_cost)
                     / ($r_quantity + $p_quantity) );
          $arr_r_prod = array(
                              'product'  => $production->Field('product'),
                              'quantity' => $r_quantity + $p_quantity,
                              'cost'     => $cost
                              );
          WebApp::execDBCmd('update_refrigerator_product', $arr_r_prod);
        }
    }
}
?>