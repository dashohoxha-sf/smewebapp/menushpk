<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    documents
 * @subpackage productions
 */
class productionList extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("productions_rs->current_page", $page);
    }
    
  function on_del($event_args)
    {
      $rs = WebApp::openRS('closed_production', $event_args);
      $is_closed = $rs->Field('is_closed');
      if ($is_closed=='true')
        {
          WebApp::message(T_("It is closed and cannot be deleted!"));
        }
      else
        {
          WebApp::execDBCmd('del_production', $event_args);
          WebApp::message(T_("Deleted."));
        }
    } 

  function onRender()
    {
      WebApp::setSVar("productions_rs->recount", "true");

      $filter_condition = $this->get_filter_condition();
      WebApp::addVar("filter_condition", $filter_condition);

      //get the variables {{total_quantity}} and {{total_value}}
      $rs_id = (PRINT_MODE=='true'?'productions_rs_print':'productions_rs');
      $rs = WebApp::openRS($rs_id);
      $total_quantity = 0;
      $total_value = 0;
      while (!$rs->EOF())
        {
          $quantity = $rs->Field('quantity');
          $cost = $rs->Field('cost');
          $total_quantity += $quantity;
          $total_value += $quantity * $cost;
          $rs->MoveNext();
        }
      WebApp::addVar('total_quantity', $total_quantity);
      WebApp::addVar('total_value', $total_value);
    }

  function get_filter_condition()
    {
      $date_filter = WebApp::getSVar("date->filter");
      $date_filter = str_replace("date_field", "production_date", $date_filter);
      $productions_filter = WebApp::getSVar("productionFilter->filter");

      $conditions[] = $date_filter;
      if ($productions_filter!='')   $conditions[] = $productions_filter;

      $filter_condition = implode(' AND ', $conditions);
      $filter_condition = "($filter_condition)";

      return $filter_condition;
    }
}
?>