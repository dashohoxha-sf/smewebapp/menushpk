
DROP TABLE IF EXISTS productions;
CREATE TABLE productions (
  production_id int(10) unsigned NOT NULL auto_increment,
  product varchar(100) default '',
  production_date date default '0000-00-00',
  quantity decimal(6,1) default '0.0',
  cost decimal(8,3) default '0.000',
  ingredients text,
  notes text,
  close_date date default '0000-00-00',
  timestamp varchar(100) default '',
  PRIMARY KEY  (production_id)
) TYPE=MyISAM;

