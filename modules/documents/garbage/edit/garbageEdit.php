<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


include_once FORM_PATH.'formWebObj.php';

/**
 * @package    documents
 * @subpackage garbage
 */
class garbageEdit extends formWebObj
{
  function init()
    {
      $this->addSVar('mode', 'add');  //add or edit
      $this->addSVar('garbage_id', UNDEFINED);
      $this->addSVar('closed', 'false');
    }

  function onParse()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->setSVar('closed', 'false');
        }
      else
        {
          $rs = WebApp::openRS('get_garbage');
          $close_date = $rs->Field('close_date');
          $closed = ($close_date==NULL_VALUE ? 'false' : 'true');
          $this->setSVar('closed', $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //add empty variables for each field in the table
          $garbage = $this->pad_record(array(), 'garbage');
          WebApp::addVars($garbage);

          //title
          $title = T_("New Garbage");
        }
      else if ($mode=='edit')
        {
          $rs = WebApp::openRS('get_garbage');
          $fields = $rs->Fields();
          WebApp::addVars($fields);
          //title
          $garbage_id = $fields['garbage_id'];
          $title = T_("Garbage").": $garbage_id";
          //modification_date
          $timestamp = $fields['timestamp'];
          $modification_date = date('d/m/Y', $timestamp);
          WebApp::addVar('modification_date', $modification_date);
        }

      //add listbox garbage_types
      $garbage_types = array(
                             'expired product' => T_("expired product"),
                             'bad product'     => T_("bad product")
                             );
      $this->add_listbox_rs('garbage_types', $garbage_types);

      WebApp::addVar('title', $title);
    }

  function on_save($event_args)
    {
      $record = $event_args;
      $record['timestamp'] = time();
      if ($record['garbage_date']=='')
        $record['garbage_date'] = get_curr_date('Y-m-d');

      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->add_new_garbage($record);
        }
      else //mode=='edit'
        {
          $record['garbage_id'] = $this->getSVar('garbage_id');
          $this->update_record($record, 'garbage', 'garbage_id');
        }
    }

  /** create a new garbage with almost the same data as this one */
  function on_clone($event_args)
    {
      $rs = WebApp::openRS('get_garbage');
      $record = $rs->Fields();
      unset($record['garbage_id']);
      $this->add_new_garbage($record);
    }

  function on_close($event_args)
    {
      $garbage = WebApp::openRS('get_garbage');
      $type = $garbage->Field('garbage_type');

      if ($type=='bad product')  //remove the product from the refrigerator
        {
          $product = $garbage->Field('product');
          $r_prod = WebApp::openRS('get_refrigerator_product',
                                   array('product' => $product) );
          if (!$r_prod->EOF())
            {
              $r_quantity = $r_prod->Field('quantity');
              $g_quantity = $garbage->Field('quantity');
              $arr_r_prod = array(
                                  'product'  => $product,
                                  'quantity' => $r_quantity - $g_quantity
                                  );
              WebApp::execDBCmd('update_refrigerator_product', $arr_r_prod);
            }
        }

      //close the document
      $params['close_date'] = get_curr_date('Y-M-D');
      WebApp::execDBCmd('close_garbage', $params);
    }

  function add_new_garbage($garbage)
    {
      //add the new garbage
      $garbage['close_date'] = 'NULL';
      $this->insert_record($garbage, 'garbage');

      //after a project is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar('mode', 'edit');

      //set 'garbage_rs->recount' to 'true'
      //so that the records are counted again
      //and the new garbage is displayed at the end of the list
      WebApp::setSVar('garbage_rs->recount', 'true');

      //set the new garbage_id
      $rs = WebApp::openRS('get_last_garbage_id');
      $garbage_id = $rs->Field('last_garbage_id');
      $this->setSVar('garbage_id', $garbage_id);
    }
}
?>