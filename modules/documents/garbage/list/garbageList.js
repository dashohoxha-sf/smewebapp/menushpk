// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function view_garbage(garbage_id)
{
  select('documents/garbage/edit', 'garbage_id='+garbage_id);
}

function print_garbage(garbage_id)
{
  print_template('documents/garbage/edit', 'garbage_id='+garbage_id);
}

function del_garbage(garbage_id, nr)
{
  var msg = T_("Deleting the garbage nr") + ': ' + nr;
  if (!confirm(msg))  return;

  SendEvent('garbageList', 'del', 'garbage_id='+garbage_id);
}
