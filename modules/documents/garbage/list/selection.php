<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    documents
 * @subpackage garbage
 */
class selection extends WebObject
{
  function onRender()
    {
      $day1 = WebApp::getSVar('date->day1');
      $month1 = WebApp::getSVar('date->month1');
      $year1 = WebApp::getSVar('date->year1');
      $day2 = WebApp::getSVar('date->day2');
      $month2 = WebApp::getSVar('date->month2');
      $year2 = WebApp::getSVar('date->year2');
      $period = "$day1/$month1/$year1 -- $day2/$month2/$year2";
      WebApp::addVar('period', $period);

      $fields = '';

      $vars = WebApp::getSVars('garbageFilter');
      array_walk($vars, 'trim');
      extract($vars);

      $arr_fields = array();
      if ($garbage_type!='')
        $arr_fields[] = '('.T_("Type")." ~ '$garbage_type')";
      if ($product!='')
        $arr_fields[] = '('.T_("Product")." ~ '$product')";
      if ($quantity!='')
        $arr_fields[] = '('.T_("Quantity")." $quantity)";
      if ($value!='')
        $arr_fields[] = '('.T_("Value")." $value)";

      $fields = implode(' '.T_("AND").' ', $arr_fields);
      WebApp::addVar('fields', $fields);
    }
}
?>