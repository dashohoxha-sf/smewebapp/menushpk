
DROP TABLE IF EXISTS garbage;
CREATE TABLE garbage (
  garbage_id int(10) unsigned NOT NULL auto_increment,
  garbage_date date default '0000-00-00',
  close_date date default '0000-00-00',
  timestamp varchar(250) NOT NULL default '',
  garbage_type varchar(100) default '',
  product varchar(100) default '',
  quantity decimal(6,1) default '0.0',
  value decimal(6,1) default '0.0',
  notes text,
  PRIMARY KEY  (garbage_id)
) TYPE=MyISAM;

