
DROP TABLE IF EXISTS sellings;
CREATE TABLE sellings (
  selling_id int(10) unsigned NOT NULL auto_increment,
  selling_date date default '0000-00-00',
  timestamp varchar(250) NOT NULL default '',
  close_date date default '0000-00-00',
  product varchar(100) default '',
  quantity decimal(6,1) default '0.0',
  price decimal(6,1) default '0.0',
  partner varchar(100) default '',
  notes text,
  PRIMARY KEY  (selling_id)
) TYPE=MyISAM;

