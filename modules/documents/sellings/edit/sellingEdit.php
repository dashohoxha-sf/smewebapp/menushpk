<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


include_once FORM_PATH.'formWebObj.php';

/**
 * @package    documents
 * @subpackage sellings
 */
class sellingEdit extends formWebObj
{
  function init()
    {
      $this->addSVar('mode', 'add');  //add or edit
      $this->addSVar('selling_id', UNDEFINED);
      $this->addSVar('closed', 'false');
    }

  function onParse()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->setSVar('closed', 'false');
        }
      else
        {
          $rs = WebApp::openRS('get_selling');
          $close_date = $rs->Field('close_date');
          $closed = ($close_date==NULL_VALUE ? 'false' : 'true');
          $this->setSVar('closed', $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //add empty variables for each field in the table
          $selling = $this->pad_record(array(), 'sellings');
          WebApp::addVars($selling);

          //title
          $title = T_("New Selling");
        }
      else if ($mode=='edit')
        {
          $rs = WebApp::openRS('get_selling');
          $fields = $rs->Fields();
          WebApp::addVars($fields);

          //title
          $selling_id = $fields['selling_id'];
          $title = T_("Selling").": $selling_id";

          //payment_id
          $rs = WebApp::openRS('get_selling_payment');
          $payment_id = $rs->Field('payment_id');
          if ($payment_id==UNDEFINED)  $payment_id = '';
          WebApp::addVar('payment_id', $payment_id);

          //modification_date
          $timestamp = $fields['timestamp'];
          $modification_date = date('d/m/Y', $timestamp);
          WebApp::addVar('modification_date', $modification_date);
        }

      WebApp::addVar('title', $title);
    }

  function on_save($event_args)
    {
      $record = $event_args;
      $record['timestamp'] = time();
      if ($record['selling_date']=='')
        $record['selling_date'] = get_curr_date('Y-m-d');

      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->add_new_selling($record);
        }
      else //mode=='edit'
        {
          $record['selling_id'] = $this->getSVar('selling_id');
          $this->update_record($record, 'sellings', 'selling_id');
        }
    }

  /** create a new selling with almost the same data as this one */
  function on_clone($event_args)
    {
      $rs = WebApp::openRS('get_selling');
      $record = $rs->Fields();
      unset($record['selling_id']);
      $this->add_new_selling($record);
    }

  function add_new_selling($selling)
    {
      //add the new selling
      $selling['close_date'] = 'NULL';
      $this->insert_record($selling, 'sellings');

      //after a project is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar('mode', 'edit');

      //set 'sellings_rs->recount' to 'true'
      //so that the records are counted again
      //and the new selling is displayed at the end of the list
      WebApp::setSVar('sellings_rs->recount', 'true');

      //set the new selling_id
      $rs = WebApp::openRS('get_last_selling_id');
      $selling_id = $rs->Field('last_selling_id');
      $this->setSVar('selling_id', $selling_id);

      //add a new payment
      if ($selling['add_payment']=='true')
        {
          $this->on_add_payment();
        }
      else
        {
          //get last payment id
          $rs = WebApp::openRS('get_last_payment');
          $payment_id = $rs->Field('last_payment');

          //add a record in doc2payment
          WebApp::execDBCmd('add_doc2payment', compact('payment_id'));
        }
    }

  /** create a new payment that is attached to this selling */
  function on_add_payment($event_args =array())
  {
    //get some values that will be added in the new payment
    $rs = WebApp::openRS('get_selling');
    $args = array(
                  'type'         => 'selling',
                  'payment_date' => $rs->Field('selling_date'),
                  'last_date'    => $rs->Field('selling_date'),
                  'partner'      => $rs->Field('partner'),
                  'percentage_payed' => '100',
                  'timestamp'    => time()
                  );

    //add a new payment
    WebApp::execDBCmd('add_payment', $args);

    //get the id of the new payment
    $rs = WebApp::openRS('get_payment_id', $args);
    $payment_id = $rs->Field('payment_id');

    //add a record in doc2payment
    WebApp::execDBCmd('add_doc2payment', compact('payment_id'));
  }

  /** change the payment that is attached to this selling */
  function on_change_payment($event_args)
  {
    $payment_id = trim($event_args['new_payment_id']);

    if ($payment_id=='')
      {
        WebApp::execDBCmd('delete_doc2payment');
      }
    else
      {
        //check that a payment with this payment_id exists
        $rs = WebApp::openRS('get_payment', compact('payment_id'));
        if ($rs->EOF())
          {
            $msg = T_("There is no payment with id 'v_payment_id'");
            $msg = str_replace('v_payment_id', $payment_id, $msg);
            WebApp::message($msg);
            return;
          }

        //update payment_id for this selling
        WebApp::execDBCmd('update_doc2payment', compact('payment_id'));
      }

    //if the old payment is not referenced by any other docs, delete it
    $payment_id = $event_args['old_payment_id'];
    $rs = WebApp::openRS('get_doc2payment', compact('payment_id'));
    if ($rs->EOF())
      {
        WebApp::execDBCmd('del_payment', compact('payment_id'));
      }
  }

  function on_close($event_args)
    {
      $selling = WebApp::openRS('get_selling');
      $r_prod = WebApp::openRS('get_refrigerator_product',
                               array('product'=>$selling->Field('product')));
      if (!$r_prod->EOF())
        {
          $r_quantity = $r_prod->Field('quantity');
          $s_quantity = $selling->Field('quantity');
          $arr_r_prod = array(
                              'product'  => $selling->Field('product'),
                              'quantity' => $r_quantity - $s_quantity
                              );
          WebApp::execDBCmd('update_refrigerator_product', $arr_r_prod);
        }

      //close the document
      $params['close_date'] = get_curr_date('Y-m-d');
      WebApp::execDBCmd('close_selling', $params);
    }
}
?>