<!--# -*-SQL-*- tell emacs to use SQL mode #-->

<Recordset ID="products">
  <Query>SELECT product AS id, product AS label FROM products</Query>
</Recordset>

<Recordset ID="partners">
  <Query>SELECT nickname AS id, nickname AS label FROM partners</Query>
</Recordset>
