<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package documents
 */
class documents extends WebObject
{
  function init()
    {
      $this->addSVar('module', 'purchases/list/list.html');
    }

  function onParse()
    {
      global $event;
      if ($event->target=='main' and $event->name=='select')
        {
          $this->select_documents($event->args);
        }
    }

  function select_documents($event_args)
    {
      $interface = WebApp::getSVar('interface');
      switch ($interface)
        {
          //purchases
        case 'documents/purchases/list':
          $interface_title = T_("Purchase").'-->'.T_("List");
          $module = 'purchases/list/list.html';
          break;
        case 'documents/purchases/edit':
          $interface_title = T_("Purchase").'-->'.T_("Edit");
          $module = 'purchases/edit/purchaseEdit.html';
          //set the state of the purchaseEdit module
          WebApp::setSVar('purchaseEdit->mode', 'edit');
          WebApp::setSVar('purchaseEdit->purchase_id', $event_args['purchase_id']);
          break;
        case 'documents/purchases/new':
          $interface_title = T_("Purchase").'-->'.T_("New");
          $module = 'purchases/edit/purchaseEdit.html';
          //set the state of the purchaseEdit module
          WebApp::setSVar('purchaseEdit->mode', 'add');
          WebApp::setSVar('purchaseEdit->purchase_id', UNDEFINED);
          break;

          //sellings
        case 'documents/sellings/list':
          $interface_title = T_("Selling").'-->'.T_("List");
          $module = 'sellings/list/list.html';
          break;
        case 'documents/sellings/edit':
          $interface_title = T_("Selling").'-->'.T_("Edit");
          $module = 'sellings/edit/sellingEdit.html';
          //set the state of the sellingEdit module
          WebApp::setSVar('sellingEdit->mode', 'edit');
          WebApp::setSVar('sellingEdit->selling_id', $event_args['selling_id']);
          break;
        case 'documents/sellings/new':
          $interface_title = T_("Selling").'-->'.T_("New");
          $module = 'sellings/edit/sellingEdit.html';
          //set the state of the sellingEdit module
          WebApp::setSVar('sellingEdit->mode', 'add');
          WebApp::setSVar('sellingEdit->selling_id', UNDEFINED);
          break;

          //payments
        case 'documents/payments/list':
          $interface_title = T_("Payment").'-->'.T_("List");
          $module = 'payments/list/list.html';
          break;
        case 'documents/payments/edit':
          $interface_title = T_("Payment").'-->'.T_("Edit");
          $module = 'payments/edit/paymentEdit.html';
          //set the state of the paymentEdit module
          WebApp::setSVar('paymentEdit->mode', 'edit');
          WebApp::setSVar('paymentEdit->payment_id', $event_args['payment_id']);
          break;
        case 'documents/payments/new':
          $interface_title = T_("Payment").'-->'.T_("New");
          $module = 'payments/edit/paymentEdit.html';
          //set the state of the paymentEdit module
          WebApp::setSVar('paymentEdit->mode', 'add');
          WebApp::setSVar('paymentEdit->payment_id', UNDEFINED);
          break;

          //productions
        case 'documents/productions/list':
          $interface_title = T_("Production").'-->'.T_("List");
          $module = 'productions/list/list.html';
          break;
        case 'documents/productions/edit':
          $interface_title = T_("Production").'-->'.T_("Edit");
          $module = 'productions/edit/productionEdit.html';
          //set the state of the productionEdit module
          WebApp::setSVar('productionEdit->mode', 'edit');
          WebApp::setSVar('productionEdit->production_id', $event_args['production_id']);
          break;
        case 'documents/productions/new':
          $interface_title = T_("Production").'-->'.T_("New");
          $module = 'productions/edit/productionEdit.html';
          //set the state of the productionEdit module
          WebApp::setSVar('productionEdit->mode', 'add');
          WebApp::setSVar('productionEdit->production_id', UNDEFINED);
          break;

          //garbage
        case 'documents/garbage/list':
          $interface_title = T_("Garbage").'-->'.T_("List");
          $module = 'garbage/list/list.html';
          break;
        case 'documents/garbage/edit':
          $interface_title = T_("Garbage").'-->'.T_("Edit");
          $module = 'garbage/edit/garbageEdit.html';
          //set the state of the garbageEdit module
          WebApp::setSVar('garbageEdit->mode', 'edit');
          WebApp::setSVar('garbageEdit->garbage_id', $event_args['garbage_id']);
          break;
        case 'documents/garbage/new':
          $interface_title = T_("Garbage").'-->'.T_("New");
          $module = 'garbage/edit/garbageEdit.html';
          //set the state of the garbageEdit module
          WebApp::setSVar('garbageEdit->mode', 'add');
          WebApp::setSVar('garbageEdit->garbage_id', UNDEFINED);
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);
      $this->setSVar('module', $module);
    }
}
?>