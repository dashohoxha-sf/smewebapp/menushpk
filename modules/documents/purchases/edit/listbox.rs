<!--# -*-SQL-*- tell emacs to use SQL mode #-->

<Recordset ID="items">
  <Query>SELECT item AS id, item AS label FROM items</Query>
</Recordset>

<Recordset ID="partners">
  <Query>SELECT nickname AS id, nickname AS label FROM partners</Query>
</Recordset>
