<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


include_once FORM_PATH.'formWebObj.php';

/**
 * @package    documents
 * @subpackage purchases
 */
class purchaseEdit extends formWebObj
{
  function init()
    {
      $this->addSVar('mode', 'add');  //add or edit
      $this->addSVar('purchase_id', UNDEFINED);
      $this->addSVar('closed', 'false');
    }

  function onParse()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->setSVar('closed', 'false');
        }
      else
        {
          $rs = WebApp::openRS('get_purchase');
          $close_date = $rs->Field('close_date');
          $closed = ($close_date==NULL_VALUE ? 'false' : 'true');
          $this->setSVar('closed', $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //add empty variables for each field in the table
          $purchase = $this->pad_record(array(), 'purchases');
          WebApp::addVars($purchase);

          //title
          $title = T_("New Purchase");
        }
      else if ($mode=='edit')
        {
          $rs = WebApp::openRS('get_purchase');
          $fields = $rs->Fields();
          WebApp::addVars($fields);

          //title
          $purchase_id = $fields['purchase_id'];
          $title = T_("Purchase").": $purchase_id";

          //payment_id
          $rs = WebApp::openRS('get_purchase_payment');
          $payment_id = $rs->Field('payment_id');
          if ($payment_id==UNDEFINED)  $payment_id = '';
          WebApp::addVar('payment_id', $payment_id);

          //modification_date
          $timestamp = $fields['timestamp'];
          $modification_date = date('d/m/Y', $timestamp);
          WebApp::addVar('modification_date', $modification_date);
        }

      WebApp::addVar('title', $title);
    }

  function on_save($event_args)
    {
      $record = $event_args;
      $record['timestamp'] = time();
      if ($record['purchase_date']=='')
        $record['purchase_date'] = get_curr_date('Y-m-d');

      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->add_new_purchase($record);
        }
      else //mode=='edit'
        {
          $record['purchase_id'] = $this->getSVar('purchase_id');
          $this->update_record($record, 'purchases', 'purchase_id');
        }
    }

  /** create a new purchase with almost the same data as this one */
  function on_clone($event_args)
    {
      $rs = WebApp::openRS('get_purchase');
      $record = $rs->Fields();
      unset($record['purchase_id']);
      $this->add_new_purchase($record);
    }

  function add_new_purchase($purchase)
    {
      //add the new purchase
      $purchase['close_date'] = 'NULL';
      $this->insert_record($purchase, 'purchases');

      //after a project is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar('mode', 'edit');

      //set 'purchases_rs->recount' to 'true'
      //so that the records are counted again
      //and the new purchase is displayed at the end of the list
      WebApp::setSVar('purchases_rs->recount', 'true');

      //set the new purchase_id
      $rs = WebApp::openRS('get_last_purchase_id');
      $purchase_id = $rs->Field('last_purchase_id');
      $this->setSVar('purchase_id', $purchase_id);

      //add a new payment
      if ($purchase['add_payment']=='true')
        {
          $this->on_add_payment();
        }
      else
        {
          //get last payment id
          $rs = WebApp::openRS('get_last_payment');
          $payment_id = $rs->Field('last_payment');

          //add a record in doc2payment
          WebApp::execDBCmd('add_doc2payment', compact('payment_id'));
        }
    }

  /** create a new payment that is attached to this purchase */
  function on_add_payment($event_args =array())
  {
    //get some values that will be added in the new payment
    $rs = WebApp::openRS('get_purchase');
    $args = array(
                  'type'         => 'purchase',
                  'payment_date' => $rs->Field('purchase_date'),
                  'last_date'    => $rs->Field('purchase_date'),
                  'partner'      => $rs->Field('partner'),
                  'percentage_payed' => '100',
                  'timestamp'    => time()
                  );

    //add a new payment
    WebApp::execDBCmd('add_payment', $args);

    //get the id of the new payment
    $rs = WebApp::openRS('get_payment_id', $args);
    $payment_id = $rs->Field('payment_id');

    //add a record in doc2payment
    WebApp::execDBCmd('add_doc2payment', compact('payment_id'));
  }

  /** change the payment that is attached to this purchase */
  function on_change_payment($event_args)
  {
    $payment_id = trim($event_args['new_payment_id']);

    if ($payment_id=='')
      {
        WebApp::execDBCmd('delete_doc2payment');
      }
    else
      {
        //check that a payment with this payment_id exists
        $rs = WebApp::openRS('get_payment', compact('payment_id'));
        if ($rs->EOF())
          {
            $msg = T_("There is no payment with id 'v_payment_id'");
            $msg = str_replace('v_payment_id', $payment_id, $msg);
            WebApp::message($msg);
            return;
          }

        //update payment_id for this purchase
        WebApp::execDBCmd('update_doc2payment', compact('payment_id'));
      }

    //if the old payment is not referenced by any other docs, delete it
    $payment_id = $event_args['old_payment_id'];
    $rs = WebApp::openRS('get_doc2payment', compact('payment_id'));
    if ($rs->EOF())
      {
        WebApp::execDBCmd('del_payment', compact('payment_id'));
      }
  }

  function on_close($event_args)
    {
      $purchase = WebApp::openRS('get_purchase');
      if ($purchase->Field('type')=='ingredient')   //add to warehouse
        $this->add_to_warehouse($purchase);
      else if ($purchase->Field('type')=='resell')  //add to refrigerator
        $this->add_to_refrigerator($purchase);
      else  return;

      $params['close_date'] = get_curr_date('Y-m-d');
      WebApp::message($params['close_date']);
      WebApp::execDBCmd('close_purchase', $params);
    }

  function add_to_warehouse($purchase)
    {
      $w_item = WebApp::openRS('get_warehouse_item',
                               array('item' => $purchase->Field('item')) );
      if ($w_item->EOF())
        {
          WebApp::execDBCmd('add_warehouse_item', $purchase->Fields());
        }
      else
        {
          $w_quantity = $w_item->Field('quantity');
          $p_quantity = $purchase->Field('quantity');
          $w_price = $w_item->Field('price');
          $p_price = $purchase->Field('price');
          $price = ( ($w_quantity * $w_price + $p_quantity * $p_price)
                     / ($w_quantity + $p_quantity) );
          $arr_w_item = array(
                              'item'     => $purchase->Field('item'),
                              'quantity' => $w_quantity + $p_quantity,
                              'price'    => $price
                              );
          WebApp::execDBCmd('update_warehouse_item', $arr_w_item);
        }
    }

  function add_to_refrigerator($purchase)
    {
      $r_prod = WebApp::openRS('get_refrigerator_product',
                               array('product' => $purchase->Field('item')) );
      if ($r_prod->EOF())
        {
          WebApp::execDBCmd('add_refrigerator_product', $purchase->Fields());
        }
      else
        {
          $r_quantity =  $r_prod->Field('quantity');
          $p_quantity =  $purchase->Field('quantity');
          $r_cost =  $r_prod->Field('cost');
          $p_price =  $purchase->Field('price');
          $cost = ( ($r_quantity * $r_cost + $p_quantity * $p_price)
                     / ($r_quantity + $p_quantity) );
          $arr_r_prod = array(
                              'item'     => $purchase->Field('item'),
                              'quantity' => $r_quantity + $p_quantity,
                              'cost'    => $cost
                              );
          WebApp::execDBCmd('update_refrigerator_product', $arr_r_prod);
        }
    }
}
?>