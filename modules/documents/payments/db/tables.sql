-- MySQL dump 8.23
--
-- Host: localhost    Database: menushpk
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS payments;
CREATE TABLE payments (
  payment_id int(10) unsigned NOT NULL auto_increment,
  payment_date date default '0000-00-00',
  last_date date default '0000-00-00',
  partner varchar(100) default '',
  type varchar(100) default '',
  amount double default '0',
  percentage_payed smallint(6) default '0',
  close_date date default '0000-00-00',
  timestamp varchar(100) default '',
  notes tinytext,
  PRIMARY KEY  (payment_id)
) TYPE=MyISAM;

