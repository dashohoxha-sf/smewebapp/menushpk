<!--# -*-SQL-*- tell emacs to use SQL mode #-->

<Recordset ID="partners">
  <Query>SELECT nickname AS id, nickname AS label FROM partners</Query>
</Recordset>

<Recordset ID="payment_types">
  <Query>SELECT type_id AS id, type_name AS label FROM payment_types</Query>
</Recordset>
