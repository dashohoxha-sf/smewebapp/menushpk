<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    documents
 * @subpackage payments
 */
class paymentFilter extends WebObject
{
  function init()
    {
      $this->addSVars( array(
                             'partner'          => '',
                             'amount'           => '',
                             'type'             => '',
                             'percentage_payed' => ''
                             ));
      $this->addSVar('filter', '');
    }

  function onParse()
    {
      $this->setSVar('filter', $this->get_filter());
    }

  function get_filter()
    {
      $vars = $this->getSVars();
      array_walk($vars, 'trim');
      extract($vars);

      $conditions = array();

      if ($partner!='')
        $conditions[] = "partner LIKE '%$partner%'";

      if ($amount!='')
        {
          $ch = $amount[0];
          if ($ch!='=' and $ch!='<' and $ch!='>') $amount = '='.$amount;
          $conditions[] = "amount $amount";
        }

      if ($type!='')
        $conditions[] = "type LIKE '%$type%'";

      if ($percentage_payed!='')
        {
          $ch = $percentage_payed[0];
          if ($ch!='=' and $ch!='<' and $ch!='>')
            $percentage_payed = '='.$percentage_payed;
          $conditions[] = "percentage_payed $percentage_payed";
        }

      $filter = implode(' AND ', $conditions);
      if ($filter!='')  $filter = "($filter)";
      return $filter;
    }

  function onRender()
    {
      $query = "SELECT type_id AS id, type_name AS label FROM payment_types";
      $rs = new EditableRS('payment_types', $query);
      $rs->Open();
      $rs->apply('paymentFilter_translate');

      global $webPage;
      $webPage->addRecordset($rs);
    }
}

/** $fields is an associated array */
function paymentFilter_translate(&$fields)
{
  $label = $fields['label'];
  $fields['label'] = T_($label);
}
?>
