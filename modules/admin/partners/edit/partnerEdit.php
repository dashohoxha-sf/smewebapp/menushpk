<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH.'formWebObj.php';

/**
 * @package    admin
 * @subpackage partners
 */
class partnerEdit extends formWebObj
{
  var $partner_data =  array( 
                          'nickname'     => '',
                          'firstname'    => '',
                          'lastname'     => '',
                          'e_mail'       => '',
                          'phone1'       => '',
                          'phone2'       => '',
                          'address'      => '',
                          'notes'        => ''
                          );

  function init()
    {
      $this->addSVar('mode', 'add');    // add | edit
    }

  /** add the new partner in database */
  function on_add($event_args)
    {
      $this->partner_data = array_merge($this->partner_data, $event_args);

      //check that such a nickname does not exist in DB
      $nickname = $event_args['nickname'];
      $rs = WebApp::openRS('get_partner_id', array('nickname'=>$nickname));
      if (!$rs->EOF())
        {
          $msg = T_("The nickname 'var_nickname' is already used for \n"
            . "somebody else.  Please choose another nickname.");
          $msg = str_replace('var_nickname', $nickname, $msg);
          WebApp::message($msg);
          $this->partner_data['nickname'] = '';
          return;
        }

      //add the partner
      WebApp::execDBCmd('add_partner', $this->partner_data);

      //get the id of the just added partner
      $rs = WebApp::openRS('get_partner_id', array('nickname'=>$nickname));
      $partner_id = $rs->Field('partner_id');

      //set the new partner as current partner and change the mode to edit
      WebApp::setSVar('partnerList->current_partner', $partner_id);
      $this->setSVar('mode', 'edit');
    }

  /** delete the current partner */
  function on_delete($event_args)
    {
      WebApp::execDBCmd('delete_partner');

      //the current_partner is deleted,
      //set current the first partner in the list
      $partnerList = WebApp::getObject('partnerList');
      $partnerList->selectFirst();

      //acknowledgment message
      WebApp::message('Partner deleted.');
    }

  /** save the changes */
  function on_save($event_args)
    {
      //update partner data
      WebApp::execDBCmd('update_partner', $event_args);
    }

  function onParse()
    {
      //get the current partner from the list of partners
      $partner = WebApp::getSVar('partnerList->current_partner');

      if ($partner==UNDEFINED)
        {
          $this->setSVar('mode', 'add');
        }
      else
        {
          $this->setSVar('mode', 'edit');
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $partner_data = $this->partner_data; 
        }
      else
        {
          $rs = WebApp::openRS('current_partner');
          $partner_data = $rs->Fields();
        }
      WebApp::addVars($partner_data);      
    }
}
?>