<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    admin
 * @subpackage partners
 */
class partnerList extends WebObject
{
  function init()
    {
      $this->addSVar("current_partner", UNDEFINED);      
      //set current the first partner in the list
      $this->selectFirst();  
    }

  /** set current_partner as the first partner in the list */
  function selectFirst()
    {  
      $rs = WebApp::openRS("selected_partners");
      $first_partner = $rs->Field("partner_id");
      $this->setSVar("current_partner", $first_partner);
    }

  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("selected_partners->current_page", $page);
      $this->selectFirst();  
    }

  function on_refresh($event_args)
    {
      //recount the selected partners
      WebApp::setSVar("selected_partners->recount", "true");

      //select the first one in the current page
      $this->selectFirst();
    }

  function on_select($event_args)
    {
      //set current_partner to the selected one
      $rs = WebApp::openRS("get_partner", $event_args);
      if ($rs->EOF())
        {
          $this->selectFirst();   
        }
      else
        {
          $partner = $event_args["partner_id"];
          $this->setSVar("current_partner", $partner);
        }
    }

  function on_add_new_partner($event_args)
    {
      //when the button Add New partner is clicked
      //make current_partner UNDEFINED
      $this->setSVar("current_partner", UNDEFINED);
    }

  function onParse()
    {
      //recount the selected partners
      WebApp::setSVar("selected_partners->recount", "true");
    }
}
?>