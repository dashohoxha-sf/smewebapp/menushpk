-- MySQL dump 8.23
--
-- Host: localhost    Database: menushpk
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS partners;
CREATE TABLE partners (
  partner_id int(11) NOT NULL auto_increment,
  nickname varchar(20) default NULL,
  firstname varchar(20) NOT NULL default '',
  lastname varchar(20) default NULL,
  e_mail varchar(25) default NULL,
  phone1 varchar(20) default NULL,
  phone2 varchar(20) default NULL,
  address varchar(35) default NULL,
  notes text,
  PRIMARY KEY  (partner_id)
) TYPE=MyISAM;

