<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    reports
 * @subpackage payments
 */

class rptPayments extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args['page'];
      WebApp::setSVar('rptPaymentPartners_rs->current_page', $page);
    }
    
  function onRender()
    {
      WebApp::setSVar('rptPaymentPartners_rs->recount', 'true');
      $this->build_report_rs();
    }

  /** 
   * Build the recordset that will be displayed in the report
   * and add it to the webPage. Calculate also the {{total_value}}.
   */
  function build_report_rs()
    {
      //get the recordset that will be used (PagedRS or not)
      $rs_id = ( PRINT_MODE=='true' ?
                 'rptPaymentPartners_rs_print' :
                 'rptPaymentPartners_rs' );

      //get the partners that will be displayed in the report
      $var_name = 'rptPaymentFilter->filter_condition';
      $filter_condition = WebApp::getSVar($var_name);
      $params['filter_condition'] = $filter_condition;
      $partners_rs = WebApp::openRS($rs_id, $params);

      //add some additional columns that will be displayed in report
      $partners_rs->addCol('amount', 0.0);
      $partners_rs->addCol('payed', 0.0);
      $partners_rs->addCol('percent', 0.0);

      //get the Payments that will be processed
      $arr_partners = $partners_rs->getColumn('partner');
      $partner_list = "'" . implode("', '", $arr_partners) . "'";
      $params['partner_list'] = $partner_list;
      $Payments_rs = WebApp::openRS('get_Payments', $params);

      //sum up the amount of the payments for each partner
      while (!$partners_rs->EOF())
        {
          $partner = $partners_rs->Field('partner');
          while ($Payments_rs->Field('partner') < $partner)
            {
              $Payments_rs->MoveNext();
            }
          while ($Payments_rs->Field('partner') == $partner)
            {
              $amount = $Payments_rs->Field('amount');
              $percent = $Payments_rs->Field('percentage_payed');

              $partner_amount = $partners_rs->Field('amount');
              $partners_rs->setFld('amount', $partner_amount + $amount);
              $payed = $partners_rs->Field('payed');
              $partners_rs->setFld('payed', $payed + $amount*$percent/100);

              $Payments_rs->MoveNext();
            }
          $partners_rs->MoveNext();
        }

      //get the total_amount, total_payed, and the average payed percentage
      $total_value = 0;
      $partners_rs->MoveFirst();
      while (!$partners_rs->EOF())
        {
          $amount = $partners_rs->Field('amount');
          $payed = $partners_rs->Field('payed');

          $percent = ($payed / $amount) * 100;
          $partners_rs->setFld('percent', $percent);

          $total_amount += $amount;
          $total_payed += $payed;

          $partners_rs->MoveNext();
        }
      if ($total_amount != 0)
        $avg_percent = ($total_payed / $total_amount) * 100;

      WebApp::addVar('total_amount', $total_amount);
      WebApp::addVar('total_payed', $total_payed);
      WebApp::addVar('avg_percent', $avg_percent);

      global $webPage;
      $webPage->addRecordset($partners_rs);
    }
}
?>