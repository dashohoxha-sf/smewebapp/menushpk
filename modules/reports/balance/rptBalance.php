<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    reports
 * @subpackage balance
 */

class rptBalance extends WebObject
{
  function onRender()
    {
      $this->add_period();

      //get the date filter
      $date_filter = WebApp::getSVar("date->filter");
      $date_filter = str_replace("date_field", "payment_date", $date_filter);
      $query_args['filter_condition'] = $date_filter;

      //get the value of sellings
      $query_args['type'] = 'selling';
      $rs = WebApp::openRS('get_payment_total', $query_args);
      $sellings = $rs->Field('total_amount');

      //get the value of purchases
      $query_args['type'] = 'purchase';
      $rs = WebApp::openRS('get_payment_total', $query_args);
      $purchases = $rs->Field('total_amount');
      $purchases = (-1)*$purchases;

      //general expenses
      $general_expenses = $this->get_general_expenses($query_args);

      //garbage
      $garbage = $this->get_garbage();

      //balance
      $balance = $sellings + $purchases + $garbage + $general_expenses;

      //add variables
      WebApp::addVars(compact('purchases', 'sellings',
                              'garbage', 'general_expenses', 'balance'));
    }

  /** add the period variable */
  function add_period()
    {
      $day1 = WebApp::getSVar('date->day1');
      $month1 = WebApp::getSVar('date->month1');
      $year1 = WebApp::getSVar('date->year1');
      $day2 = WebApp::getSVar('date->day2');
      $month2 = WebApp::getSVar('date->month2');
      $year2 = WebApp::getSVar('date->year2');

      $period = "$day1/$month1/$year1 -- $day2/$month2/$year2";
      WebApp::addVar('period', $period);
    }

  function get_general_expenses($query_args)
    {
      $query_args['type'] = 'salary';
      $rs = WebApp::openRS('get_payment_total', $query_args);
      $salary = $rs->Field('total_amount');

      $query_args['type'] = 'other';
      $rs = WebApp::openRS('get_payment_total', $query_args);
      $other = $rs->Field('total_amount');

      $general_expenses = $salary + $other;
      $general_expenses = (-1)*$general_expenses;

      return $general_expenses;
    }

  function get_garbage()
    {
      //get the date filter
      $date_filter = WebApp::getSVar("date->filter");
      $date_filter = str_replace("date_field", "garbage_date", $date_filter);
      $query_args['filter_condition'] = $date_filter;

      $rs = WebApp::openRS('get_garbage_total', $query_args);
      $garbage = $rs->Field('total_amount');
      $garbage = (-1) * $garbage;

      return $garbage;
    }
}
?>