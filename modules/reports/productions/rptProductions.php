<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    reports
 * @subpackage productions
 */

class rptProductions extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args['page'];
      WebApp::setSVar('rptProductionProducts_rs->current_page', $page);
    }
    
  function onRender()
    {
      WebApp::setSVar('rptProductionProducts_rs->recount', 'true');
      $this->build_report_rs();
    }

  /** 
   * Build the recordset that will be displayed in the report
   * and add it to the webPage. Calculate also the {{total_value}}.
   */
  function build_report_rs()
    {
      //get the recordset that will be used (PagedRS or not)
      $rs_id = ( PRINT_MODE=='true' ?
                 'rptProductionProducts_rs_print' :
                 'rptProductionProducts_rs' );

      //get the products that will be displayed in the report
      $var_name = 'rptProductionFilter->filter_condition';
      $filter_condition = WebApp::getSVar($var_name);
      $params['filter_condition'] = $filter_condition;
      $products_rs = WebApp::openRS($rs_id, $params);

      //add some additional columns that will be displayed in report
      $products_rs->addCol('quantity', 0.0);
      $products_rs->addCol('cost', 0.0);
      $products_rs->addCol('value', 0.0);

      //get the Productions that will be processed
      $arr_products = $products_rs->getColumn('product');
      $product_list = "'" . implode("', '", $arr_products) . "'";
      $params['product_list'] = $product_list;
      $Productions_rs = WebApp::openRS('get_Productions', $params);

      //sum up the quantity and the values of the Productions for each product
      while (!$products_rs->EOF())
        {
          $product = $products_rs->Field('product');
          while ($Productions_rs->Field('product') < $product)
            {
              $Productions_rs->MoveNext();
            }
          while ($Productions_rs->Field('product') == $product)
            {
              $quantity = $Productions_rs->Field('quantity');
              $cost = $Productions_rs->Field('cost');

              $product_quantity = $products_rs->Field('quantity');
              $products_rs->setFld('quantity', $product_quantity + $quantity);
              $product_value = $products_rs->Field('value');
              $products_rs->setFld('value', $product_value + $quantity*$cost);

              $Productions_rs->MoveNext();
            }
          $products_rs->MoveNext();
        }

      //calculate the average cost for each product, the total value, etc.
      $total_value = 0;
      $products_rs->MoveFirst();
      while (!$products_rs->EOF())
        {
          //calculate the average cost
          $quantity = $products_rs->Field('quantity');
          $value = $products_rs->Field('value');
          $cost = ($quantity==0 ? 0 : $value/$quantity);
          $cost = round($cost*100)/100;
          $products_rs->setFld('cost', $cost);

          //total value
          $total_value += $value;

          $products_rs->MoveNext();
        }

      WebApp::addVar('total_value', $total_value);

      global $webPage;
      $webPage->addRecordset($products_rs);
    }
}
?>


