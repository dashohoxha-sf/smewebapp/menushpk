<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package reports
 */
class reports extends WebObject
{
  function init()
  {
    $this->addSVar('module', 'purchases/rptPurchases.html');
  }

  function onParse()
    {
      global $event;
      if ($event->target=='main' and $event->name=='select')
        {
          $this->select($event->args);
        }
    }

  function select($event_args)
    {
      $interface = WebApp::getSVar('interface');
      switch ($interface)
        {
          //purchases
        case 'reports/purchases':
          $interface_title = T_("Reports").'-->'.T_("Purchases");
          $module = 'purchases/rptPurchases.html';
          break;

          //productions
        case 'reports/productions':
          $interface_title = T_("Reports").'-->'.T_("Productions");
          $module = 'productions/rptProductions.html';
          break;

          //sellings
        case 'reports/sellings':
          $interface_title = T_("Reports").'-->'.T_("Sellings");
          $module = 'sellings/rptSellings.html';
          break;

          //payments
        case 'reports/payments':
          $interface_title = T_("Reports").'-->'.T_("Payments");
          $module = 'payments/rptPayments.html';
          break;

          //garbage
        case 'reports/garbage':
          $interface_title = T_("Reports").'-->'.T_("Garbage");
          $module = 'garbage/rptGarbages.html';
          break;

          //balance
        case 'reports/balance':
          $interface_title = T_("Reports").'-->'.T_("Balance");
          $module = 'balance/rptBalance.html';
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);
      $this->setSVar('module', $module);
    }
}
?>
