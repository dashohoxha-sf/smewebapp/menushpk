<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    reports
 * @subpackage garbage
 */

class rptGarbages extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args['page'];
      WebApp::setSVar('rptGarbageProducts_rs->current_page', $page);
    }
    
  function onRender()
    {
      WebApp::setSVar('rptGarbageProducts_rs->recount', 'true');
      $this->build_report_rs();
    }

  /** 
   * Build the recordset that will be displayed in the report
   * and add it to the webPage. Calculate also the {{total_value}}.
   */
  function build_report_rs()
    {
      //get the recordset that will be used (PagedRS or not)
      $rs_id = ( PRINT_MODE=='true' ?
                 'rptGarbageProducts_rs_print' :
                 'rptGarbageProducts_rs' );

      //get the products that will be displayed in the report
      $var_name = 'rptGarbageFilter->filter_condition';
      $filter_condition = WebApp::getSVar($var_name);
      $params['filter_condition'] = $filter_condition;
      $products_rs = WebApp::openRS($rs_id, $params);

      //get the Garbages that will be processed
      $arr_products = $products_rs->getColumn('product');
      $product_list = "'" . implode("', '", $arr_products) . "'";
      $params['product_list'] = $product_list;
      $Garbages_rs = WebApp::openRS('get_Garbages', $params);

      //add some additional columns that will be displayed in report
      $products_rs->addCol('garbage_type', '');
      $products_rs->addCol('quantity', 0.0);
      $products_rs->addCol('value', 0.0);

      //copy the sum_quantity and the sum_value for each product
      while (!$products_rs->EOF())
        {
          $garbage_type = $Garbages_rs->Field('garbage_type');
          $products_rs->setFld('garbage_type', $garbage_type);
          $sum_quantity = $Garbages_rs->Field('sum_quantity');
          $products_rs->setFld('quantity', $sum_quantity);
          $sum_value = $Garbages_rs->Field('sum_value');
          $products_rs->setFld('value', $sum_value);

          $products_rs->MoveNext();
          $Garbages_rs->MoveNext();
        }

      //translate some fields of the recordset
      $products_rs->apply('rptGarbages_translate');

      //get the total value, etc.
      $total_value = 0;
      $products_rs->MoveFirst();
      while (!$products_rs->EOF())
        {
          $value = $products_rs->Field('value');
          $total_value += $value;
          $products_rs->MoveNext();
        }
      WebApp::addVar('total_value', $total_value);

      global $webPage;
      $webPage->addRecordset($products_rs);
    }
}

function rptGarbages_translate(&$fields)
{
  $type = $fields['garbage_type'];
  if ($type!='') $type = T_($type);
  $fields['garbage_type'] = $type;
}
?>