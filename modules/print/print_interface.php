<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package print
 */
class print_interface extends WebObject
{
  function init()
    {
      $this->addSVar("print_file", UNDEFINED);
    }

  function onRender()
    {
      WebApp::addVar("interface_title", $this->title);

      //modules can use this constant in order to change
      //their display so that it is more suitable for printing
      define("PRINT_MODE", "true");
    }

  function on_select($event_args)
    {
      $this->title = WebApp::getSVar("interface_title");
      $interface = $event_args["interface"];
      switch ($interface)
        {
        default:
          $this->title = WebApp::getSVar("interface_title");
          $print_file = WebApp::getSVar("module");
          break;

          //documents
        case 'documents/purchases/list':
          $print_file = "documents/purchases/list/purchaseList.html";
          break;
        case 'documents/sellings/list':
          $print_file = "documents/sellings/list/sellingList.html";
          break;
        case 'documents/payments/list':
          $print_file = "documents/payments/list/paymentList.html";
          break;
        case 'documents/productions/list':
          $print_file = "documents/productions/list/productionList.html";
          break;
        case 'documents/garbage/list':
          $print_file = "documents/garbage/list/garbageList.html";
          break;

          //inventory
        case 'inventory/warehouse':
          $print_file = "inventory/warehouse/print/wItemPrint.html";
          break;
        case 'inventory/refrigerator':
          $print_file = "inventory/refrigerator/print/rProductPrint.html";
          break;

          //reports
        case 'reports/purchases':
          $print_file = "reports/purchases/print/rptPurchasePrint.html";
          break;
        case 'reports/sellings':
          $print_file = "reports/sellings/print/rptSellingPrint.html";
          break;
        case 'reports/garbage':
          $print_file = "reports/garbage/print/rptGarbagePrint.html";
          break;
        case 'reports/productions':
          $print_file = "reports/productions/print/rptProductionPrint.html";
          break;
        case 'reports/payments':
          $print_file = "reports/payments/print/rptPaymentPrint.html";
          break;

          //tables
        case 'tables/items':
          $print_file = "tables/items/edit/itemEdit.html";
          break;
        case 'tables/products':
          $print_file = "tables/products/edit/productEdit.html";
          break;
        case 'tables/general_expenses':
          $print_file = "tables/general_expenses/print/expensePrint.html";
          break;
          //simple_tables
        case 'tables/simple_tables/group1':
        case 'tables/simple_tables/group2':
        case 'tables/simple_tables/group3':
          $print_file = 'tables/simple_tables/print/tablePrint.html';
          break;

          //admin
        case 'admin/users':
          $print_file = "admin/users/edit/userEdit.html";
          break;
        case 'admin/partners':
          $print_file = "admin/partners/edit/partnerEdit.html";
          break;
        }
      WebApp::setSVar("print_file", $print_file);
    }
}
?>
