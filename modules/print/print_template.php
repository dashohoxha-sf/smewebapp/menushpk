<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package print
 */
class print_template extends WebObject
{
  function init()
    {
      $this->addSVar("print_file", UNDEFINED);
    }

  function on_select($event_args)
    {
      $interface = $event_args["interface"];
      switch ($interface)
        {
        default:
          $this->title = WebApp::getSVar('interface_title');
          $print_file = WebApp::getSVar("module");
          break;

          //documents
        case 'documents/purchases/new':
        case 'documents/purchases/edit':
          $this->title = T_("Purchase");
          $print_file = "documents/purchases/print/purchasePrint.html";
          $purchase_id = $event_args["purchase_id"];
          WebApp::setSVar("purchasePrint->purchase_id", $purchase_id);
          break;
        case 'documents/sellings/new':
        case 'documents/sellings/edit':
          $this->title = T_("Selling");
          $print_file = "documents/sellings/print/sellingPrint.html";
          $selling_id = $event_args["selling_id"];
          WebApp::setSVar("sellingPrint->selling_id", $selling_id);
          break;
        case 'documents/payments/new':
        case 'documents/payments/edit':
          $this->title = T_("Payment");
          $print_file = "documents/payments/print/paymentPrint.html";
          $payment_id = $event_args["payment_id"];
          WebApp::setSVar("paymentPrint->payment_id", $payment_id);
          break;
        case 'documents/productions/new':
        case 'documents/productions/edit':
          $this->title = T_("Production");
          $print_file = "documents/productions/print/productionPrint.html";
          $production_id = $event_args["production_id"];
          WebApp::setSVar("productionPrint->production_id", $production_id);
          break;
        case 'documents/garbage/new':
        case 'documents/garbage/edit':
          $this->title = T_("Garbage");
          $print_file = "documents/garbage/print/garbagePrint.html";
          $garbage_id = $event_args["garbage_id"];
          WebApp::setSVar("garbagePrint->garbage_id", $garbage_id);
          break;

          //tables
        case 'tables/items':
          $this->title = T_("Items");
          $print_file = "tables/items/print/itemsPrint.html";
          break;
        case 'tables/products':
          $this->title = T_("Products");
          $print_file = "tables/products/print/productsPrint.html";
          break;

          //admin
        case 'admin/users':
          $this->title = T_("Users");
          $print_file = "admin/users/print/usersPrint.html";
          break;
        case 'admin/partners':
          $this->title = T_("Partners");
          $print_file = "admin/partners/print/partnersPrint.html";
          break;

          //Magazina
        case 'magazina/leter/faturat/listo':
        case 'magazina/boje/faturat/listo':
          $print_file = "magazina/faturat/listo/faturList.html";
          break;
        case 'magazina/leter/levizjet':
        case 'magazina/boje/levizjet':
          $print_file = "magazina/levizjet/levizjeList.html";
          break;
        case 'magazina/faturat/printo':
          $magazin_id = $event_args['magazin_id'];
          $fature_id = $event_args['fature_id'];
          $hyrje_dalje = $event_args['lloj_fature'];

          $this->title = "Magazina ".ucfirst($magazin_id)
            . "-->Fature ".ucfirst($hyrje_dalje);
          $print_file = "magazina/faturat/edit/faturEdit.html";

          WebApp::setSVar('magazina->id', $magazin_id);
          WebApp::setSVar('faturEdit->fature_id', $fature_id);
          WebApp::setSVar('faturEdit->hyrje_dalje', $hyrje_dalje);
          WebApp::setSVar('faturEdit->mode', 'edit');
          break;
        }
      WebApp::setSVar("print_file", $print_file);
      WebApp::addGlobalVar("title", $this->title);
  }
}
?>
