// -*-C-*- 

var MENU_ITEMS =
[
  ['Shenimet', null, null,
    ['Blerjet', 'javascript:select(\'documents/purchases/list\')', null],
    ['Prodhimet', 'javascript:select(\'documents/productions/list\')', null],
    ['Shitjet', 'javascript:select(\'documents/sellings/list\')', null],
    ['Pagesat', 'javascript:select(\'documents/payments/list\')', null],
    ['Skarcot', 'javascript:select(\'documents/garbage/list\')', null],
   ],
  ['Gjendjet', null, null,
    ['Magazina', 'javascript:select(\'inventory/warehouse\')', null],
    ['Frigoriferi', 'javascript:select(\'inventory/refrigerator\')', null],
   ],
  ['Tabelat', null, null,
    ['Lenda e Pare', 'javascript:select(\'tables/items\')', null],
    ['Produktet', 'javascript:select(\'tables/products\')', null],
    ['Shpenzime', 'javascript:select(\'tables/general_expenses\')', null],
   ],
  ['Admin', null, null,
    ['Punetoret', 'javascript:select(\'admin/users\')', null],
    ['Klientet', 'javascript:select(\'admin/partners\')', null],
    ['Backup/Restore', 'javascript:select(\'backup\')', null],
   ],
 ];
