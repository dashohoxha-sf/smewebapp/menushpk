
DROP TABLE IF EXISTS refrigerator;
CREATE TABLE refrigerator (
  product varchar(20) NOT NULL default '',
  unit varchar(10) default NULL,
  quantity decimal(7,2) default NULL,
  cost decimal(7,2) default NULL,
  PRIMARY KEY  (product)
) TYPE=MyISAM COMMENT='The table that contains .';

