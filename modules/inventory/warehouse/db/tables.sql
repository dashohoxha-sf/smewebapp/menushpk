
DROP TABLE IF EXISTS warehouse;
CREATE TABLE warehouse (
  item varchar(20) NOT NULL default '',
  unit varchar(10) default NULL,
  quantity decimal(7,2) default NULL,
  min_quantity decimal(6,1) default NULL,
  max_quantity decimal(6,1) default NULL,
  price decimal(7,2) default NULL,
  PRIMARY KEY  (item)
) TYPE=MyISAM COMMENT='The table that contains the inventory of the warehouse.';

