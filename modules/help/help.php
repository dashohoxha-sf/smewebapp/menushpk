<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/** don't append webnotes at the end of each section */
define('WEBNOTES_ENABLE', 'false');

/**
 * @package help
 */
class help extends WebObject
{
  function init()
    {
      $cache_path = UP_PATH.'smewebapp-docs/content/books/cache/';
      WebApp::setSVar('docbook->cache_path', $cache_path);
      WebApp::setSVar('docbook->book_id', 'menushpk_manual');
      WebApp::setSVar('docbook->node_path', './');
      WebApp::setSVar('docbook->lng', 'en');
    }

  function onParse()
    {
      global $event;
      if ($event->target=='main' and $event->name=='select')
        {
          $topic = $event->args['topic'];
          $node_path = $this->get_node_path($topic);
          WebApp::setSVar('docbook->node_path', $node_path);
        }
    }

  function afterParse()
    {
      global $webPage;
      $path = dirname(__FILE__);
      $url = WebApp::to_url($path);
      $css = "  <link rel=\"stylesheet\" href=\"$url/help.css\" type=\"text/css\" />\n";
      $webPage->append_to_head($css);
    }

  function get_node_path($topic)
    {
      switch ($topic)
        {
        default:
          $node_path = './';
          break;

          //documents
        case 'documents/purchases/list':
        case 'documents/purchases/edit':
        case 'documents/purchases/new':
          $node_path = './documents/purchases/';
          break;
        case 'documents/sellings/list':
        case 'documents/sellings/edit':
        case 'documents/sellings/new':
          $node_path = './documents/sellings/';
          break;
        case 'documents/payments/list':
        case 'documents/payments/edit':
        case 'documents/payments/new':
          $node_path = './documents/payments/';
          break;
        case 'documents/productions/list':
        case 'documents/productions/edit':
        case 'documents/productions/new':
          $node_path = './documents/productions/';
          break;
        case 'documents/garbage/list':
        case 'documents/garbage/edit':
        case 'documents/garbage/new':
          $node_path = './documents/garbage/';
          break;

          //inventory
        case 'inventory/warehouse':
          $node_path = './inventory/warehouse/';
          break;
        case 'inventory/refrigerator':
          $node_path = './inventory/warehouse/';
          break;

          //reports
        case 'reports/purchases':
        case 'reports/productions':
        case 'reports/sellings':
        case 'reports/garbage':
        case 'reports/payments':
        case 'reports/balance':
          $node_path = './reports/';
          break;

          //tables
        case 'tables/items':
        case 'tables/products':
        case 'tables/general_expenses':
        case 'tables/simple_tables/group1':
        case 'tables/simple_tables/group2':
        case 'tables/simple_tables/group3':
          $node_path = './tables/';
          break;

        case 'superuser':
          $node_path = './';
          break;
        case 'user_profile':
          $node_path = './';
          break;
        case 'change_date':
          $node_path = './';
          break;
        case 'admin/users':
          $node_path = './';
          break;
        case 'backup':
          $node_path = './';
          break;
        case 'help':
          $node_path = './';
          break;
        }

      return $node_path;
    }
}
?>
