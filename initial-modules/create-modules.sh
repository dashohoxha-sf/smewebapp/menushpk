#!/bin/bash
### create the initial modules

### go to this directory
cd $(dirname $0)

### package 'documents'
./init-module.sh documents documents payments
./init-module.sh documents documents productions
./init-module.sh documents documents purchases
./init-module.sh documents documents sellings
./init-module.sh documents documents garbage

### package 'admin'
./init-module.sh users admin users
./init-module.sh users admin partners

### package 'inventory'
./init-module.sh items inventory warehouse
./init-module.sh items inventory refrigerator

### package 'reports'
./init-module.sh summary_report reports purchases
./init-module.sh summary_report reports sellings
./init-module.sh summary_report reports garbage
./init-module.sh summary_report reports productions
./init-module.sh summary_report reports payments

### package 'tables'
./init-module.sh items_1 tables items
./init-module.sh items_1 tables products
./init-module.sh items tables general_expenses
./init-module.sh tables tables simple_tables

