#!/bin/bash
### update all the modules

### go to this directory
cd $(dirname $0)

### package 'documents'
./update-module.sh documents documents payments
./update-module.sh documents documents productions
./update-module.sh documents documents purchases
./update-module.sh documents documents sellings
./update-module.sh documents documents garbage

### package 'admin'
./update-module.sh users admin users
./update-module.sh users admin partners

### package 'inventory'
./update-module.sh items inventory warehouse
./update-module.sh items inventory refrigerator

### package 'reports'
./update-module.sh summary_report reports payments
./update-module.sh summary_report reports productions
./update-module.sh summary_report reports purchases
./update-module.sh summary_report reports sellings
./update-module.sh summary_report reports garbage

### package 'tables'
./update-module.sh items_1 tables items
./update-module.sh items_1 tables products
./update-module.sh items tables general_expenses
./update-module.sh tables tables simple_tables

