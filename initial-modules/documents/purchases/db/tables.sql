
DROP TABLE IF EXISTS purchases;
CREATE TABLE purchases (
  purchase_id int(10) unsigned NOT NULL auto_increment,
  purchase_date date default '0000-00-00',
  timestamp varchar(100) NOT NULL,
  field_1 varchar(100) default '',
  field_2 varchar(100) default '',
  field_3 varchar(100) default '',
  field_4 varchar(100) default '',
  notes text default '',
  close_date date default '0000-00-00',
  PRIMARY KEY  (purchase_id)
) TYPE=MyISAM;

