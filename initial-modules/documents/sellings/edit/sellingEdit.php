<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


include_once FORM_PATH.'formWebObj.php';

/**
 * @package    documents
 * @subpackage sellings
 */
class sellingEdit extends formWebObj
{
  function init()
    {
      $this->addSVar('mode', 'add');  //add or edit
      $this->addSVar('selling_id', UNDEFINED);
      $this->addSVar('closed', 'false');
    }

  function onParse()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->setSVar('closed', 'false');
        }
      else
        {
          $rs = WebApp::openRS('get_selling');
          $close_date = $rs->Field('close_date');
          $closed = ($close_date==NULL_VALUE ? 'false' : 'true');
          $this->setSVar('closed', $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //add empty variables for each field in the table
          $selling = $this->pad_record(array(), 'sellings');
          WebApp::addVars($selling);

          //title
          $title = T_("New selling");
        }
      else if ($mode=='edit')
        {
          $rs = WebApp::openRS('get_selling');
          $fields = $rs->Fields();
          WebApp::addVars($fields);
          //title
          $selling_id = $fields['selling_id'];
          $title = T_("selling").": $selling_id";
          //modification_date
          $timestamp = $fields['timestamp'];
          $modification_date = date('d/m/Y', $timestamp);
          WebApp::addVar('modification_date', $modification_date);
        }

      WebApp::addVar('title', $title);
    }

  function on_save($event_args)
    {
      $record = $event_args;
      $record['timestamp'] = time();
      if ($record['selling_date']=='')
        $record['selling_date'] = get_curr_date('Y-m-d');

      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->add_new_selling($record);
        }
      else //mode=='edit'
        {
          $record['selling_id'] = $this->getSVar('selling_id');
          $this->update_record($record, 'sellings', 'selling_id');
        }
    }

  /** create a new selling with almost the same data as this one */
  function on_clone($event_args)
    {
      $rs = WebApp::openRS('get_selling');
      $record = $rs->Fields();
      unset($record['selling_id']);
      $this->add_new_selling($record);
    }

  function on_close($event_args)
    {
      $params['close_date'] = get_curr_date('Y-M-D');
      WebApp::execDBCmd('close_selling', $params);
    }

  function add_new_selling($selling)
    {
      //add the new selling
      $selling['close_date'] = 'NULL';
      $this->insert_record($selling, 'sellings');

      //after a project is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar('mode', 'edit');

      //set 'sellings_rs->recount' to 'true'
      //so that the records are counted again
      //and the new selling is displayed at the end of the list
      WebApp::setSVar('sellings_rs->recount', 'true');

      //set the new selling_id
      $rs = WebApp::openRS('get_last_selling_id');
      $selling_id = $rs->Field('last_selling_id');
      $this->setSVar('selling_id', $selling_id);
    }
}
?>