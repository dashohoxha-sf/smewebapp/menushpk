<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


include_once FORM_PATH.'formWebObj.php';

/**
 * @package    documents
 * @subpackage payments
 */
class paymentEdit extends formWebObj
{
  function init()
    {
      $this->addSVar('mode', 'add');  //add or edit
      $this->addSVar('payment_id', UNDEFINED);
      $this->addSVar('closed', 'false');
    }

  function onParse()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->setSVar('closed', 'false');
        }
      else
        {
          $rs = WebApp::openRS('get_payment');
          $close_date = $rs->Field('close_date');
          $closed = ($close_date==NULL_VALUE ? 'false' : 'true');
          $this->setSVar('closed', $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //add empty variables for each field in the table
          $payment = $this->pad_record(array(), 'payments');
          WebApp::addVars($payment);

          //title
          $title = T_("New payment");
        }
      else if ($mode=='edit')
        {
          $rs = WebApp::openRS('get_payment');
          $fields = $rs->Fields();
          WebApp::addVars($fields);
          //title
          $payment_id = $fields['payment_id'];
          $title = T_("payment").": $payment_id";
          //modification_date
          $timestamp = $fields['timestamp'];
          $modification_date = date('d/m/Y', $timestamp);
          WebApp::addVar('modification_date', $modification_date);
        }

      WebApp::addVar('title', $title);
    }

  function on_save($event_args)
    {
      $record = $event_args;
      $record['timestamp'] = time();
      if ($record['payment_date']=='')
        $record['payment_date'] = get_curr_date('Y-m-d');

      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $this->add_new_payment($record);
        }
      else //mode=='edit'
        {
          $record['payment_id'] = $this->getSVar('payment_id');
          $this->update_record($record, 'payments', 'payment_id');
        }
    }

  /** create a new payment with almost the same data as this one */
  function on_clone($event_args)
    {
      $rs = WebApp::openRS('get_payment');
      $record = $rs->Fields();
      unset($record['payment_id']);
      $this->add_new_payment($record);
    }

  function on_close($event_args)
    {
      $params['close_date'] = get_curr_date('Y-M-D');
      WebApp::execDBCmd('close_payment', $params);
    }

  function add_new_payment($payment)
    {
      //add the new payment
      $payment['close_date'] = 'NULL';
      $this->insert_record($payment, 'payments');

      //after a project is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar('mode', 'edit');

      //set 'payments_rs->recount' to 'true'
      //so that the records are counted again
      //and the new payment is displayed at the end of the list
      WebApp::setSVar('payments_rs->recount', 'true');

      //set the new payment_id
      $rs = WebApp::openRS('get_last_payment_id');
      $payment_id = $rs->Field('last_payment_id');
      $this->setSVar('payment_id', $payment_id);
    }
}
?>