<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package    tables
 * @subpackage products
 */
class productList extends WebObject
{
  function init()
    {
      $this->addSVar("selected", UNDEFINED);      
      //set current the first product in the list
      $this->select_first();  
    }

  /** set selected as the first product in the list */
  function select_first()
    {  
      $rs = WebApp::openRS("products_rs");
      $first = $rs->Field("product");
      $this->setSVar("selected", $first);
    }

  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("products_rs->current_page", $page);
      $this->select_first();  
    }

  function on_refresh($event_args)
    {
      //recount the selected products
      WebApp::setSVar("products_rs->recount", "true");

      //select the first one in the current page
      $this->select_first();
    }

  function on_select($event_args)
    {
      //set selected to the selected one
      $rs = WebApp::openRS("get_product", $event_args);
      if ($rs->EOF())
        {
          $this->select_first();   
        }
      else
        {
          $product = $event_args["product"];
          $this->setSVar("selected", $product);
        }
    }

  function on_add($event_args)
    {
      //when the button Add New product is clicked
      //make selected UNDEFINED
      $this->setSVar("selected", UNDEFINED);
    }

  function onParse()
    {
      //recount the selected products
      WebApp::setSVar("products_rs->recount", "true");
    }
}
?>