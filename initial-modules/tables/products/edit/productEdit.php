<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003,2004,2005,2006 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH.'formWebObj.php';

/**
 * @package    tables
 * @subpackage products
 */
class productEdit extends formWebObj
{
  var $product_data =  array( 
                          'product'   => '',
                          'unit'     => '',
                          'price'    => '',
                          'type'     => '',
                          'partners' => '',
                          'notes'    => ''
                          );

  function init()
    {
      $this->addSVar('mode', 'add');    // add | edit
    }

  /** add the new product in database */
  function on_add($event_args)
    {
      $this->product_data = array_merge($this->product_data, $event_args);

      //check that such an product does not exist in DB
      $product = $event_args['product'];
      $rs = WebApp::openRS('get_product', array('product'=>$product));
      if (!$rs->EOF())
        {
          $msg = T_("The product 'var_product' is already used for \n"
            . "somebody else.  Please choose another product.");
          $msg = str_replace('var_product', $product, $msg);
          WebApp::message($msg);
          $this->product_data['product'] = '';
          return;
        }

      //add the product
      WebApp::execDBCmd('add_product', $this->product_data);

      //set the new product as selected and change the mode to edit
      WebApp::setSVar('productList->selected', $product);
      $this->setSVar('mode', 'edit');
    }

  /** delete the current product */
  function on_delete($event_args)
    {
      WebApp::execDBCmd('delete_product');

      //the selected product is deleted,
      //select the first product in the list
      $productList = WebApp::getObject('productList');
      $productList->select_first();

      //acknowledgment message
      WebApp::message(T_("Item deleted."));
    }

  /** save the changes */
  function on_save($event_args)
    {
      //update product data
      WebApp::execDBCmd('update_product', $event_args);
    }

  function onParse()
    {
      //get the current product from the list of products
      $product = WebApp::getSVar('productList->selected');

      if ($product==UNDEFINED)
        {
          $this->setSVar('mode', 'add');
        }
      else
        {
          $this->setSVar('mode', 'edit');
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $product_data = $this->product_data; 
        }
      else
        {
          $rs = WebApp::openRS('selected');
          $product_data = $rs->Fields();
        }
      WebApp::addVars($product_data);      
    }
}
?>